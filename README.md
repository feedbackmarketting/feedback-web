Designed and Developed by Balaji Thennarangam.

Contact: balaji.thennarangam@gmail.com / balaji.t123@gmail.com

ShowMyFeedback.com application collects feedback from users about the products, services and people.
The feedback can be sought or given by the users.