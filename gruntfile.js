module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        ngAnnotate: {
            options: {
                singleQuotes: true
            },
            app: { //"app" target
                files: {
                    './min-safe/app/app.js': ['./app/app.js'],
                    './min-safe/app/controllers/IndexController.js': ['./app/controllers/IndexController.js'],
                    './min-safe/app/controllers/HomeController.js': ['./app/controllers/HomeController.js'],
                    './min-safe/app/controllers/UserRegistrationController.js': ['./app/controllers/UserRegistrationController.js'],
                    './min-safe/app/controllers/LoginController.js': ['./app/controllers/LoginController.js'],
                    './min-safe/app/controllers/UserHomeController.js': ['./app/controllers/UserHomeController.js'],
                    './min-safe/app/controllers/AccountSetupController.js': ['./app/controllers/AccountSetupController.js'],
                    './min-safe/app/controllers/ProductConfigController.js': ['./app/controllers/ProductConfigController.js'],
                    './min-safe/app/controllers/SeekFeedbackController.js': ['./app/controllers/SeekFeedbackController.js'],
                    './min-safe/app/controllers/ProvideVoluntaryFeedbackController.js': ['./app/controllers/ProvideVoluntaryFeedbackController.js'],
                    './min-safe/app/controllers/ProvideFeedbackController.js': ['./app/controllers/ProvideFeedbackController.js'],
                    './min-safe/app/services/NavigationFactory.js': ['./app/services/NavigationFactory.js'],
                    './min-safe/app/services/UserRegistrationService.js': ['./app/services/UserRegistrationService.js'],
                    './min-safe/app/services/ProductService.js': ['./app/services/ProductService.js'],
                    './min-safe/app/services/FeedbackService.js': ['./app/services/FeedbackService.js'],
                    './min-safe/app/services/Rating.js': ['./app/services/Rating.js'],
                    './min-safe/app/services/angularjs-crypto.js': ['./app/services/angularjs-crypto.js'],
                    './min-safe/app/services/CryptoJSCipher.js': ['./app/services/CryptoJSCipher.js']
                }
            }
        },
        concat: {
            js: { //target
                src: ['./min-safe/app/app.js','./min-safe/app/controllers/*.js','./min-safe/app/services/*.js'],
                dest: './js/feedbackme.min.js'
            }
        },
        uglify: {
            js: { //target
                src: ['./js/feedbackme.min.js'],
                dest: './js/feedbackme.min.js'
            }
        }



        //grunt task configuration will go here     
    });
    //load grunt task
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-ng-annotate');
    //register grunt default task
    grunt.registerTask('default', ['ngAnnotate', 'concat', 'uglify']);
}
