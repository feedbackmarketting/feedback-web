<?php
include("../php/helper.php");
$configs = FeedbackHelper::getInitConfiguration();
$baseUrl = $configs["baseUrl"];
$dbserver = $configs["dbserver"];
$dbname = $configs["dbname"];
$dbuser = $configs["dbuser"];
$dbpwd = $configs["dbpwd"];
$request = $_GET;

//print_r($request);
if($request){
		
try{
	$errorObject = null;
	if($request["email"] && $request["key"]){
		
		$email = $request["email"];
		$key = $request["key"];
		if($errorObject){
			//http_response_code(500);
			error_log(json_encode($errorObject));
			echo json_encode($errorObject);
		}else{			
			$con = mysqli_connect("$dbserver","$dbuser","$dbpwd","$dbname");

			if(mysqli_connect_error()){
				error_log(mysqli_connect_error());
				$errorObject = FeedbackHelper::constructErrorObject($errorObject,"DB_CONNECT_ERROR","Could not connect to database");
				echo json_encode($errorObject);
				//http_response_code(500);
				//echo $errorObject;	
			}else{
				$query = "SELECT `activation_key` FROM `users` where `email` = '$email'";
				
				//echo("qry is $query");	
				$result = mysqli_query($con,$query);
				if($result->num_rows >0){
					$storedActivationKey = null;
					while($row = $result->fetch_assoc()){
						$storedActivationKey = $row["activation_key"];
						//echo "activation key is : ".$row["activation_key"];
					}
					if($storedActivationKey && $storedActivationKey == $key){
						$query_update = "UPDATE `users` set `active`=true where `email` = '$email'";
						mysqli_query($con,$query_update);
						
						//echo("Activation successful! Please login with this <a href='/index.html#/login'>link</a>");	
						header("Location: ../index.html#/login/activation");
						exit();
					}else{
						$errorObject = FeedbackHelper::constructErrorObject($errorObject,"INVALID_CREDENTIALS","Invalid credentials");
					error_log(json_encode($errorObject));
					echo "Invalid activation key! Please try registering again!";	
					}
				}else{
					//http_response_code(500);					
					$errorObject = FeedbackHelper::constructErrorObject($errorObject,"INVALID_CREDENTIALS","Invalid credentials");
					error_log(json_encode($errorObject));
					echo "No user found! Please try registering again!";
				}
				
			}
			
		}
	}else{
		$errorObject = FeedbackHelper::constructErrorObject($errorObject,"INVALID_ACTIVATION_URL","Invalid activation link");
		error_log(json_encode($errorObject));
		echo "Invalid activation link! Please try registering again!";
	}
}catch(Exception $e){
	error_log($e);
	echo "Error occuered!";	
}
}
?>
