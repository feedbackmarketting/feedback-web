<?php
include("helper.php");
//echo("Users REST service");
$configs = FeedbackHelper::getInitConfiguration();
$baseUrl = $configs["baseUrl"];
$dbserver = $configs["dbserver"];
$dbname = $configs["dbname"];
$dbuser = $configs["dbuser"];
$dbpwd = $configs["dbpwd"];
$request = $_POST;
$getRequest = $_GET;

//print_r($request);
if($request){
		
try{
	if($request["seek"]){

		$errorObject = null;
		if(!$request["user"]){
			$errorObject = FeedbackHelper::constructErrorObject($errorObject,"USER_EMPTY","User id` must not be empty");
		}else{
			$user=$request["user"];
		}
		if(!$request["userName"]){
			$errorObject = FeedbackHelper::constructErrorObject($errorObject,"USER_NAME_EMPTY","User name must not be empty");
		}else{
			$userName=$request["userName"];
		}
		if(!$request["targetCateg"]){
			$errorObject = FeedbackHelper::constructErrorObject($errorObject,"TARGET_EMPTY","User name must not be empty");
		}else{
			$targetCateg=$request["targetCateg"];
		}
		if($targetCateg == "PRODUCT" || $targetCateg == "SERVICE"){
			if(!$request["product"]){
				$errorObject = FeedbackHelper::constructErrorObject($errorObject,"PRODUCT_EMPTY","Product must not be empty");			
			}else{
				$product=$request["product"];
			}

			if(!$request["productName"]){
				$errorObject = FeedbackHelper::constructErrorObject($errorObject,"PRODUCT_NAME_EMPTY","Product Name must not be empty");			
			}else{
				$productName=$request["productName"];
			}	
		}
		
		if(!$request["audience"]){
			$errorObject = FeedbackHelper::constructErrorObject($errorObject,"AUDIENCE_EMPTY","Audience must not be empty");	
		}else{
			$audience=$request["audience"];
		}
		if(!$request["categories"]){
			$errorObject = FeedbackHelper::constructErrorObject($errorObject,"CATEGORIES_EMPTY","Categories must not be empty");	
		}else{
			$categories=$request["categories"];
		}
		
		if($errorObject){
			//http_response_code(500);
			error_log(json_encode($errorObject));
			echo json_encode($errorObject);
			die();	
		}else{
			$con = mysqli_connect("$dbserver","$dbuser","$dbpwd","$dbname");

			if(mysqli_connect_error()){
				error_log(mysqli_connect_error());
				$errorObject = FeedbackHelper::constructErrorObject($errorObject,"DB_CONNECT_ERROR","Could not connect to database");
				echo json_encode($errorObject);
			}else{
				$query = "";
				$productId = null;
				$targetTypeId = null;
				$targetId = null;
				$targetEmailSubject = "";
				$audEmail = null;
				$catName = null;
				if($targetCateg == "PERSON"){
					$targetTypeId = "1";
					$targetId = $user;
					$targetEmailSubject = "on him.";
				}
				if($targetCateg == "ORGANIZATION"){
					$targetTypeId = "4";
					$targetId = $user;
					$targetEmailSubject = "on their organization.";
				}
				if($targetCateg == "PRODUCT"){
					$targetTypeId = "2";
					$targetId = $product;
					$targetEmailSubject = "on their product $productName.";
				}
				if($targetCateg == "SERVICE"){
					$targetTypeId = "3";
					$targetId = $product;
					$targetEmailSubject = "on their service $productName.";
				}
				
				$query = "INSERT into `seek_feedback` (`seeker`,`target_type`,`target`,`created_date`) VALUES ('$user','$targetTypeId','$targetId',sysdate())";
				//echo("qry is $query");	
				$result = mysqli_query($con,$query);
				$seekId = mysqli_insert_id($con);

				foreach ($categories as $cat){
					//echo "Category Name: ".$cat["name"];
					$catName = $cat["name"];
					//echo "Is service: $per['isService']";
					//echo "<br/>";
					
					$query = "INSERT INTO `seek_categories` (`seek_id`,`feedback_category`) VALUES ('$seekId','$catName')";
					//echo("qry is $query");	
					$result = mysqli_query($con,$query);
				}

				$subject = "ShowMyFeedback.com - $userName is requesting your feedback $targetEmailSubject";
				$body = "<pre>Dear User,\r\n";
				$body = $body."$userName is requesting your feedback $targetEmailSubject";
				$body = $body."\r\n\r\n";
				$body = $body."Please provide your feedback using the below link.";
				$body = $body."\r\n\r\n";
				//$body = $body."$baseUrl/provideFeedback?seekId=$seekId";
				$bodyEnd = "\r\n\r\n";
				$bodyEnd = $bodyEnd."Thanks and Regards...";
				$bodyEnd = $bodyEnd."\r\n\r\n";
				$bodyEnd = $bodyEnd."ShowMyFeedback.com Team</pre>";
				$from = "donotreply@feedback.me.uk";
				$fromName = "ShowMyFeedback.com";
				foreach ($audience as $per){
					//echo "Audience email: ".$per["text"];
					//echo "Is service: $per['isService']";
					//echo "<br/>";
					$audEmail = $per["text"];
					$query = "INSERT INTO `seek_audience` (`seek_id`,`email`) VALUES ('$seekId','$audEmail')";
					//echo("qry is $query");	
					$result = mysqli_query($con,$query);
					$bodyLink = "$baseUrl/#/provideFeedback/$audEmail/$seekId/$targetTypeId";
					$body = $body.$bodyLink.$bodyEnd;
					$mailSent = FeedbackHelper::sendElasticEmail("$audEmail","$subject",null,"$body","$from","$fromName");
					//echo("success! Seek feednback Mail sent status is $mailSent");
					//TODO if execpetion send notify the user.				
				}
			}
		}
	}

	//Provide Feedback
	/*
		Parmeters:
		providerEmail - mandatory
		targetCateg - mandatory
		product - mandatory
		categories - mandatory
		user - mandatory if provided from as a logged in user, optional if provided as sought user
		productName - optional - used to send an email to the seeker
		overallComment - optional user input
		seekerEmail - optional - used for sending out email
		userName - optional but mandatory when seekerEmail is provided
		seekId - oprtional but mandatory for sought feedback
	*/
	if($request["provide"]){

		$errorObject = null;
		$user = "NULL";
		$providerEmail = null;
		$overallComment = "NULL";
		$seekerEmail = null;
		
		if($request["user"]){
			$user=$request["user"];
		}
		if(!$request["providerEmail"]){
			$errorObject = FeedbackHelper::constructErrorObject($errorObject,"PROVIDER_EMAIL_EMPTY","Provider Email must not be empty");
		}else{
			$providerEmail=$request["providerEmail"];
		}
		
		if(!$request["targetCateg"]){
			$errorObject = FeedbackHelper::constructErrorObject($errorObject,"TARGET_EMPTY","Target category must not be empty");
		}else{
			$targetCateg=$request["targetCateg"];
		}
		if(!$request["product"]){
			$errorObject = FeedbackHelper::constructErrorObject($errorObject,"TARGET_EMPTY","Target Id must not be empty");
		}else{
			$product=$request["product"];
		}
		if($request["productName"]){
			$productName=$request["productName"];
		}

		if(!$request["categories"]){
			$errorObject = FeedbackHelper::constructErrorObject($errorObject,"CATEGORIES_EMPTY","Categories must not be empty");	
		}else{
			$categories=$request["categories"];
		}
		if($request["overallComment"]){
			$overallComment = $request["overallComment"];
			$overallComment = "'$overallComment'";
		}
		if($request["seekerEmail"]){
			$seekerEmail=$request["seekerEmail"];
			if(!$request["userName"]){
				$errorObject = FeedbackHelper::constructErrorObject($errorObject,"USER_NAME_EMPTY","User name must not be empty");
			}else{
				$userName=$request["userName"];
			}
		}
		if($request["seekId"]){
			$seekId=$request["seekId"];
		}
		
		if($errorObject){
			//http_response_code(500);
			error_log(json_encode($errorObject));
			echo json_encode($errorObject);
			die();	
		}else{
			
			$con = mysqli_connect("$dbserver","$dbuser","$dbpwd","$dbname");

			if(mysqli_connect_error()){
				error_log(mysqli_connect_error());
				$errorObject = FeedbackHelper::constructErrorObject($errorObject,"DB_CONNECT_ERROR","Could not connect to database");
				echo json_encode($errorObject);
				//http_response_code(500);
				//echo $errorObject;	
			}else{
				$query = "";
				$productId = null;
				$targetId = null;
				$targetEmailSubject = "";
				$audEmail = null;
				$catName = null;
				
				$query = "INSERT into `provide_feedback` (`provider`,`provider_email`,`target_type`,`target`,`comment`,`created_date`) VALUES ($user,'$providerEmail','$targetCateg','$product',$overallComment,sysdate())";
				//echo("qry is $query");	
				$result = mysqli_query($con,$query);
				$provideId = mysqli_insert_id($con);

				if($provideId >0){
					foreach ($categories as $cat){
						//echo "Category Name: ".$cat["name"];
						$catName = $cat["name"];
						$rating = $cat["rating"];
						$reason = "NULL";
						if($cat["reason"]){
							$reason = $cat["reason"];
							$reason = "'$reason'";	
						}
						//echo "<br/>";
						
						$query = "INSERT INTO `provide_categories` (`provide_id`,`feedback_category`,`rating`,`comment`) VALUES ('$provideId','$catName','$rating',$reason)";
						//echo("qry is $query");	
						$result = mysqli_query($con,$query);
					}

					if($seekId){
						$query = "UPDATE `seek_audience` set `is_complete` = '1' where `seek_id`=$seekId and `email`= '$providerEmail'";
						//echo("qry is $query");	
						$result = mysqli_query($con,$query);	
					}

					if($seekerEmail){

						$targetEmailSubject = "";
						if($targetCateg == "1"){
							$targetEmailSubject = "on you.";
						}
						if($targetCateg == "4"){
							$targetEmailSubject = "on your organization.";
						}
						if($targetCateg == "2"){
							$targetEmailSubject = "on your product $productName.";
						}
						if($targetCateg == "3"){
							$targetEmailSubject = "on your service $productName.";
						}

						$subject = "ShowMyFeedback.com - $userName has provided feedback $targetEmailSubject";
						$body = "<pre>Dear User,\r\n";
						$body = $body."$userName has provided feedback $targetEmailSubject";
						$body = $body."\r\n\r\n";
						$body = $body."Please check your account using the below link.";
						$body = $body."\r\n\r\n";
						//$body = $body."$baseUrl/provideFeedback?seekId=$seekId";
						$bodyEnd = "\r\n\r\n";
						$bodyEnd = $bodyEnd."Thanks and Regards...";
						$bodyEnd = $bodyEnd."\r\n\r\n";
						$bodyEnd = $bodyEnd."ShowMyFeedback.com Team</pre>";
						$from = "donotreply@feedback.me.uk";
						$fromName = "ShowMyFeedback.com";

						$bodyLink = "$baseUrl/#/userHome";
						$body = $body.$bodyLink.$bodyEnd;
						$mailSent = FeedbackHelper::sendElasticEmail("$seekerEmail","$subject",null,"$body","$from","$fromName");
						//echo("success! Seek feednback Mail sent status is $mailSent");	
					}
				}
				
			}
		}
	}
}catch(Exception $e){
	error_log($e);
	echo "Error occuered $e";	
}
}

if($getRequest){
		
try{
	if($getRequest["getSeekDetails"]){
		$userObject = null;
		$errorObject = null;
		if(!$getRequest["seekId"]){
			$errorObject = FeedbackHelper::constructErrorObject($errorObject,"SEEK_ID_EMPTY","Seek Id must not be empty");
		}else{
			$seekId=$getRequest["seekId"];
		}
		if(!$getRequest["targetTypeId"]){
			$errorObject = FeedbackHelper::constructErrorObject($errorObject,"TARGET_TYPE_ID_EMPTY","Target type id must not be empty");
		}else{
			$targetTypeId=$getRequest["targetTypeId"];
		}

		if($errorObject){
			//http_response_code(500);
			error_log(json_encode($errorObject));
			echo json_encode($errorObject);
		}else{
			
			$con = mysqli_connect("$dbserver","$dbuser","$dbpwd","$dbname");

			if(mysqli_connect_error()){
				error_log(mysqli_connect_error());
				$errorObject = FeedbackHelper::constructErrorObject($errorObject,"DB_CONNECT_ERROR","Could not connect to database");
				echo json_encode($errorObject);
				//http_response_code(500);
				//echo $errorObject;	
			}else{

				if($targetTypeId == "2" || $targetTypeId == "3"){
					$query = "select s.id as seekerId,u.name as seekerName,t.id as targetTypeId,t.name as targetTypeName,p.id as productId,p.name as productName, c.feedback_category as feedback_category,u.email as seekerEmail from seek_feedback s,seek_categories c, users u, feedback_targets t, products p where s.id=c.seek_id and s.seeker=u.id and s.target_type=t.id and s.target=p.id and s.id=$seekId";	
				}else{
					$query = "select s.id as seekerId,u.name as seekerName,t.id as targetTypeId,t.name as targetTypeName,u.id as productId,u.name as productName, c.feedback_category as feedback_category,u.email as seekerEmail from seek_feedback s,seek_categories c, users u, feedback_targets t where s.id=c.seek_id and s.seeker=u.id and s.target_type=t.id and s.target=u.id and s.id=$seekId";
				}
				
				//echo("qry is $query");	
				$result = mysqli_query($con,$query);
				if($result->num_rows >0){
					$seekerId = null;
					$seekerName = null;
					$seekerEmail = null;
					$targetTypeId = null;
					$targetTypeName = null;
					$productId = null;
					$productName = null;					
					$feedBackCategs = null;
					while($row = $result->fetch_assoc()){
						
						$seekerId = $row["seekerId"];
						$seekerName = $row["seekerName"];
						$targetTypeId = $row["targetTypeId"];
						$targetTypeName = $row["targetTypeName"];
						$productId = $row["productId"];
						$productName = $row["productName"];
						$seekerEmail = $row["seekerEmail"];
						$feedBackCategs = FeedbackHelper::constructFeedbackCategObject($feedBackCategs,$row["feedback_category"]);
					}
					$seekDetailObject = null;
					$seekDetailObject = FeedbackHelper::constructSeekDetailObject($seekDetailObject,$seekerId,$seekerName,$seekerEmail,$targetTypeId,$targetTypeName,$productId,$productName,$feedBackCategs);
					echo json_encode($seekDetailObject);
				}else{
					//http_response_code(500);					
					$errorObject = FeedbackHelper::constructErrorObject($errorObject,"NO_DATA_FOUND","No data found");
					error_log(json_encode($errorObject));
					echo json_encode($errorObject);
				}
				
			}
			
		}
	}

	//Fetch the feedbacks on a target type
	if($getRequest["getFeedbacks"]){
		$userObject = null;
		$errorObject = null;
		if(!$getRequest["userId"]){
			$errorObject = FeedbackHelper::constructErrorObject($errorObject,"USER_ID_EMPTY","User Id must not be empty");
		}else{
			$userId=$getRequest["userId"];
		}
		if(!$getRequest["targetTypeId"]){
			$errorObject = FeedbackHelper::constructErrorObject($errorObject,"TARGET_TYPE_ID_EMPTY","Target type id must not be empty");
		}else{
			$targetTypeId=$getRequest["targetTypeId"];
		}

		if($errorObject){
			//http_response_code(500);
			error_log(json_encode($errorObject));
			echo json_encode($errorObject);
		}else{
			
			$con = mysqli_connect("$dbserver","$dbuser","$dbpwd","$dbname");

			if(mysqli_connect_error()){
				error_log(mysqli_connect_error());
				$errorObject = FeedbackHelper::constructErrorObject($errorObject,"DB_CONNECT_ERROR","Could not connect to database");
				echo json_encode($errorObject);
				//http_response_code(500);
				//echo $errorObject;	
			}else{

				if($targetTypeId == "1" || $targetTypeId == "4"){
					$query = "select feedback.id as feedbackId,provider.name as providerName, feedback.provider_email as providerEmail,feedback.comment, DATE_FORMAT(feedback.created_date, '%d %M %Y %l %i %p') as created_date from users provider,provide_feedback feedback where provider.id=feedback.provider and feedback.target_type=$targetTypeId and feedback.target=$userId union select feedback.id as feedback_id,feedback.provider_email as provider_name, feedback.provider_email,feedback.comment, DATE_FORMAT(feedback.created_date, '%d %M %Y %l %i %p') as created_date from provide_feedback feedback where feedback.target_type=$targetTypeId and feedback.target=$userId and feedback.provider is null order by feedbackId desc";	
				}else{
					$query = "select feedback.id as feedbackId,provider.name as providerName, feedback.provider_email as providerEmail,feedback.comment,product.name as productName, DATE_FORMAT(feedback.created_date, '%d %M %Y %l %i %p') as created_date from users provider,provide_feedback feedback,products product,user_products userProduct where provider.id=feedback.provider and feedback.target_type in (2,3) and feedback.target=product.id and product.id=userProduct.product and userProduct.user=$userId union select feedback.id as feedback_id,feedback.provider_email as provider_name, feedback.provider_email,feedback.comment,product.name as productName, DATE_FORMAT(feedback.created_date, '%d %M %Y %l %i %p') as created_date from provide_feedback feedback,products product,user_products userProduct where feedback.target_type in (2,3) and feedback.target=product.id and product.id=userProduct.product and userProduct.user=$userId and feedback.provider is null order by feedbackId desc";
				}
				
				//echo("qry is $query");	
				$result = mysqli_query($con,$query);
				if($result->num_rows >0){
					$feedbackId = null;
					$providerName = null;
					$providerEmail = null;
					$comment = null;
					$targetTypeName = null;
					$productId = null;
					$productName = null;					
					$prevFeedbackId = null;
					$feedbackObject = null;
					$feedBackCategs = null;
					$feedbackDate = null;	
					while($row = $result->fetch_assoc()){

						$feedbackId = $row["feedbackId"];
						$providerName = $row["providerName"];
						$providerEmail = $row["providerEmail"];
						$comment = $row["comment"];
						$feedbackDate = $row["created_date"];
						$productName = null;
						if($targetTypeId == "2" || $targetTypeId == "3"){
							$productName = $row["productName"];
						}
						$feedBackCategs = null;

						$query = "select feedbackCateg.feedback_category,feedbackCateg.rating,feedbackCateg.comment as reason from provide_categories feedbackCateg where feedbackCateg.provide_id = $feedbackId";
						$subResult = mysqli_query($con,$query);
						
						if($subResult->num_rows >0){
							while($row = $subResult->fetch_assoc()){
								$feedBackCategs = FeedbackHelper::constructFeedbackCategRatingObject($feedBackCategs,$row["feedback_category"],$row["rating"],$row["reason"]);
							}
						}
						$feedbackObject = FeedbackHelper::constructFeedbackObject($feedbackObject,$feedbackId,$providerName,$providerEmail,$comment,$productName,$feedBackCategs,$feedbackDate);							
					}
					echo json_encode($feedbackObject);
				}else{
					//http_response_code(500);					
					$errorObject = FeedbackHelper::constructErrorObject($errorObject,"NO_DATA_FOUND","No data found");
					error_log(json_encode($errorObject));
					echo json_encode($errorObject);
				}
				
			}
			
		}
	}
}catch(Exception $e){
	error_log($e);
	echo "Error occuered $e";	
}
}
?>