<?php
include("helper.php");
//echo("Users REST service");
$configs = FeedbackHelper::getInitConfiguration();
$baseUrl = $configs["baseUrl"];
$dbserver = $configs["dbserver"];
$dbname = $configs["dbname"];
$dbuser = $configs["dbuser"];
$dbpwd = $configs["dbpwd"];
$request = $_POST;

//print_r($request);
if($request){
		
try{
	if($request["login"]){

		$errorObject = null;
		$userObject = null;
		if(!$request["email"]){
			$errorObject = FeedbackHelper::constructErrorObject($errorObject,"EMAIL_EMPTY","Email must not be empty");			
		}else{
			$email=$request["email"];
		}
		if(!$request["password_enc"]){
			$errorObject = FeedbackHelper::constructErrorObject($errorObject,"PASSWORD_EMPTY","Password must not be empty");	
		}else{
			$password=$request["password_enc"];
		}

		if($errorObject){
			//http_response_code(500);
			error_log(json_encode($errorObject));
			echo json_encode($errorObject);
		}else{
			
			$con = mysqli_connect("$dbserver","$dbuser","$dbpwd","$dbname");

			if(mysqli_connect_error()){
				error_log(mysqli_connect_error());
				$errorObject = FeedbackHelper::constructErrorObject($errorObject,"DB_CONNECT_ERROR","Could not connect to database");
				echo json_encode($errorObject);
				//http_response_code(500);
				//echo $errorObject;	
			}else{
				$query = "SELECT `id`,`name`,`is_organization` FROM `users` where `email` = '$email' AND `password` = '$password'";
				
				
				//echo("qry is $query");	
				$result = mysqli_query($con,$query);
				if($result->num_rows >0){
					$userId = null;
					$name = null;
					$is_organization = null;
					while($row = $result->fetch_assoc()){
						$userId = $row["id"];
						$name = $row["name"];
						$is_organization = $row["is_organization"];
						//echo "activation key is : ".$row["activation_key"];
					}
					if($userId && $name){
						//echo("Login successful!");

						$userObject = FeedbackHelper::constructUserObject($userObject,"$userId","$name",$is_organization,null,null);
						echo json_encode($userObject);
					}else{
						$errorObject = FeedbackHelper::constructErrorObject($errorObject,"INVALID_CREDENTIALS","Invalid credentials");
						error_log(json_encode($errorObject));
						echo json_encode($errorObject);	
					}
				}else{
					//http_response_code(500);					
					$errorObject = FeedbackHelper::constructErrorObject($errorObject,"INVALID_CREDENTIALS","Invalid credentials");
					error_log(json_encode($errorObject));
					echo json_encode($errorObject);
				}
				
			}
			
		}
	}

	if($request["sendPasswordLink"]){

		$errorObject = null;
		$userObject = null;
		if(!$request["email"]){
			$errorObject = FeedbackHelper::constructErrorObject($errorObject,"EMAIL_EMPTY","Email must not be empty");			
		}else{
			$email=$request["email"];
		}

		if($errorObject){
			//http_response_code(500);
			error_log(json_encode($errorObject));
			echo json_encode($errorObject);
			die();
		}else{			
			$con = mysqli_connect("$dbserver","$dbuser","$dbpwd","$dbname");

			if(mysqli_connect_error()){
				error_log(mysqli_connect_error());
				$errorObject = FeedbackHelper::constructErrorObject($errorObject,"DB_CONNECT_ERROR","Could not connect to database");
				echo json_encode($errorObject);
				//http_response_code(500);
				//echo $errorObject;	
			}else{
				$query = "SELECT `name`,`activation_key` FROM `users` where `email` = '$email'";
				
				
				//echo("qry is $query");	
				$result = mysqli_query($con,$query);
				if($result->num_rows >0){
					$userName = null;
					$activationKey = null;
					while($row = $result->fetch_assoc()){
						$userName = $row["name"];
						$activationKey = $row["activation_key"];
						//echo "activation key is : ".$row["activation_key"];
					}
					if($activationKey){

						$subject = "ShowMyFeedback.com - Reset Password!";
						$body = "<pre>Dear $userName,\r\n";
						$body = $body."Thanks for using ShowMyFeedback.com!";
						$body = $body."\r\n\r\n";
						$body = $body."Please reset your password using the below link.";
						$body = $body."\r\n\r\n";
						$body = $body."$baseUrl/#/resetPassword/$email/$activationKey";
						$body = $body."\r\n\r\n";
						$body = $body."Thanks and Regards...";
						$body = $body."\r\n\r\n";
						$body = $body."ShowMyFeedback.com Team</pre>";
						$from = "donotreply@feedback.me.uk";
						$fromName = "ShowMyFeedback.com";
						$mailSent = FeedbackHelper::sendElasticEmail("$email","$subject",null,"$body","$from","$fromName");
						echo "Reset password link sent";
					}else{
						$errorObject = FeedbackHelper::constructErrorObject($errorObject,"INVALID_CREDENTIALS","Invalid credentials");
						error_log(json_encode($errorObject));
						echo json_encode($errorObject);	
					}
				}else{
					//http_response_code(500);					
					$errorObject = FeedbackHelper::constructErrorObject($errorObject,"INVALID_CREDENTIALS","Invalid credentials");
					error_log(json_encode($errorObject));
					echo json_encode($errorObject);
				}
				
			}
			
		}
	}

	if($request["resetPassword"]){

		$errorObject = null;
		$userObject = null;
		if(!$request["email"]){
			$errorObject = FeedbackHelper::constructErrorObject($errorObject,"EMAIL_EMPTY","Email must not be empty");			
		}else{
			$email=$request["email"];
		}
		if(!$request["activationKey"]){
			$errorObject = FeedbackHelper::constructErrorObject($errorObject,"ACTIVATION_KEY_EMPTY","Activation key must not be empty");		
		}else{
			$activationKey=$request["activationKey"];
		}
		if(!$request["password_enc"]){
			$errorObject = FeedbackHelper::constructErrorObject($errorObject,"PASSWORD_EMPTY","Password must not be empty");		
		}else{
			$password_enc=$request["password_enc"];
		}

		if($errorObject){
			//http_response_code(500);
			error_log(json_encode($errorObject));
			echo json_encode($errorObject);
			die();
		}else{			
			$con = mysqli_connect("$dbserver","$dbuser","$dbpwd","$dbname");

			if(mysqli_connect_error()){
				error_log(mysqli_connect_error());
				$errorObject = FeedbackHelper::constructErrorObject($errorObject,"DB_CONNECT_ERROR","Could not connect to database");
				echo json_encode($errorObject);
				//http_response_code(500);
				//echo $errorObject;	
			}else{
				$query = "UPDATE `users` set `password`='$password_enc' where `email` = '$email' and `activation_key` = '$activationKey' ";
				
				
				//echo("qry is $query");	
				$result = mysqli_query($con,$query);
				if($result){
					$subject = "ShowMyFeedback.com - Your Password has been reset successfully!";
					$body = "<pre>Dear User,\r\n";
					$body = $body."Thanks for using ShowMyFeedback.com!";
					$body = $body."\r\n\r\n";
					$body = $body."Your password has been reset successfully! Please login using the below link";
					$body = $body."\r\n\r\n";
					$body = $body."$baseUrl/#/login";
					$body = $body."\r\n\r\n";
					$body = $body."Thanks and Regards...";
					$body = $body."\r\n\r\n";
					$body = $body."ShowMyFeedback.com Team</pre>";
					$from = "donotreply@feedback.me.uk";
					$fromName = "ShowMyFeedback.com";
					$mailSent = FeedbackHelper::sendElasticEmail("$email","$subject",null,"$body","$from","$fromName");
					echo "Password reset successfully";					
				}else{
					//http_response_code(500);					
					$errorObject = FeedbackHelper::constructErrorObject($errorObject,"INVALID_CREDENTIALS","Invalid credentials");
					error_log(json_encode($errorObject));
					echo json_encode($errorObject);
				}
				
			}
			
		}
	}
}catch(Exception $e){
	error_log($e);
	echo json_encode($e);	
}
}
?>