<?php
include("helper.php");
//echo("Users REST service");
$configs = FeedbackHelper::getInitConfiguration();
$baseUrl = $configs["baseUrl"];
$dbserver = $configs["dbserver"];
$dbname = $configs["dbname"];
$dbuser = $configs["dbuser"];
$dbpwd = $configs["dbpwd"];
$request = $_POST;
$getRequest = $_GET;

//print_r($request);
if($request){
		
try{
	if($request["register"]){

		$errorObject = null;
		if(!$request["userName"]){
			$errorObject = FeedbackHelper::constructErrorObject($errorObject,"USER_NAME_EMPTY","User name must not be empty");
		}else{
			$userName=urldecode($request["userName"]);
			//echo "user name in registration $userName";
		}
		if(!$request["email"]){
			$errorObject = FeedbackHelper::constructErrorObject($errorObject,"EMAIL_EMPTY","Email must not be empty");			
		}else{
			$email=urldecode($request["email"]);
		}
		if(!$request["password_enc"]){
			$errorObject = FeedbackHelper::constructErrorObject($errorObject,"PASSWORD_EMPTY","Password must not be empty");	
		}else{
			$password=$request["password_enc"];
		}
		if(!$request["termsAgreed"]){
			$errorObject = FeedbackHelper::constructErrorObject($errorObject,"TERMS_EMPTY","You must agree the terms and conditions");	
		}else{
			$termsAgreed=$request["termsAgreed"];
		}
		$mobile = null;
		if($request["mobile"]){
			$mobile=urldecode($request["mobile"]);
		}

		if($errorObject){
			//http_response_code(500);
			error_log(json_encode($errorObject));
			echo json_encode($errorObject);
		}else{
			
			$con = mysqli_connect("$dbserver","$dbuser","$dbpwd","$dbname");

			if(mysqli_connect_error()){
				error_log(mysqli_connect_error());
				$errorObject = FeedbackHelper::constructErrorObject($errorObject,"DB_CONNECT_ERROR","Could not connect to database");
				echo json_encode($errorObject);
				//http_response_code(500);
				//echo $errorObject;	
			}else{
				$query = "";
				$activationKey = FeedbackHelper::generateRandomString(32);
				if($mobile){
					$query = "INSERT INTO `users` (`name`,`email`,`mobile`,`password`,`terms_agreed`,`created_date`,`activation_key`) ";
					$query = $query."VALUES ('$userName','$email','$mobile','$password',$termsAgreed,sysdate(),'$activationKey')";	
				}else{
					$query = "INSERT INTO `users` (`name`,`email`,`password`,`terms_agreed`,`created_date`,`activation_key`) ";
					$query = $query."VALUES ('$userName','$email','$password',$termsAgreed,sysdate(),'$activationKey')";						
				}
				
				//echo("qry is $query");	
				$result = mysqli_query($con,$query);
				if($result){
					$subject = "ShowMyFeedback.com - Thanks for registering. Please activate your account!";
					$body = "<pre>Dear $userName,\r\n";
					$body = $body."Thanks for registering on ShowMyFeedback.com!";
					$body = $body."\r\n\r\n";
					$body = $body."Please activate your account using the below link.";
					$body = $body."\r\n\r\n";
					$body = $body."$baseUrl/activation?email=$email&key=$activationKey";
					$body = $body."\r\n\r\n";
					$body = $body."Thanks and Regards...";
					$body = $body."\r\n\r\n";
					$body = $body."ShowMyFeedback.com Team</pre>";
					$from = "donotreply@feedback.me.uk";
					$fromName = "ShowMyFeedback.com";
					$mailSent = FeedbackHelper::sendElasticEmail("$email","$subject",null,"$body","$from","$fromName");
					//echo("success! Mail sent status is $mailSent");
				}else{
					//http_response_code(500);
					$errMsg = mysqli_error($con);
					error_log($errMsg);
					if (strpos($errMsg, 'Duplicate') !== false && strpos($errMsg, 'email') !== false) {
   	 					$errMsg = "An user is already existing with the email address $email";
					}else if(strpos($errMsg, 'Duplicate') !== false && strpos($errMsg, 'mobile') !== false){
						$errMsg = "An user is already existing with the mobile number $mobile";
					}else{
						$errMsg = "Something has gon wrong! Please try again later";
					}				
					$errorObject = FeedbackHelper::constructErrorObject($errorObject,"DB_OPERATION_ERROR","$errMsg");
					echo json_encode($errorObject);
				}
				
			}
			
		}
	}
}catch(Exception $e){
	error_log($e);
	echo "Error occuered $e";	
}
}

if($getRequest){
		
try{
	if($getRequest["qryUser"]){
		$userObject = null;
		$errorObject = null;
		$userId = null;
		$name= null;
		$isOrg = null;
		if($getRequest["email"]){
			//$errorObject = FeedbackHelper::constructErrorObject($errorObject,"QRY_NAME_EMPTY","Query name must not be empty");
			$email=$getRequest["email"];
		}else{
			$email="";
		}
		if($getRequest["userId"]){
			$userId=$getRequest["userId"];
		}
		if($getRequest["name"]){
			$name=$getRequest["name"];
		}
		if($getRequest["isOrg"] != null){
			$isOrg=$getRequest["isOrg"];
		}

		if($errorObject){
			//http_response_code(500);
			error_log(json_encode($errorObject));
			echo json_encode($errorObject);
		}else{
			
			$con = mysqli_connect("$dbserver","$dbuser","$dbpwd","$dbname");

			if(mysqli_connect_error()){
				error_log(mysqli_connect_error());
				$errorObject = FeedbackHelper::constructErrorObject($errorObject,"DB_CONNECT_ERROR","Could not connect to database");
				echo json_encode($errorObject);
				//http_response_code(500);
				//echo $errorObject;	
			}else{
				$query = "select `id`,`name`,`email`,`mobile`,`is_organization` 
						from `users` where active=1 ";
				if($email != null && $email != ""){
					$query = $query."and email like '%$email%' ";
				}
				if($name != null && $name != ""){
					$query = $query."and name like '%$name%' ";
				}
				if($isOrg != null && $isOrg != ""){
					$query = $query."and is_organization = '$isOrg' ";
				}
				if($userId != null && $userId != ""){
					$query = $query."and id not in ('$userId')";
				}
				//echo("qry is $query");	
				$result = mysqli_query($con,$query);
				if($result->num_rows >0){
					$userId = null;
					$userName = null;
					$emailId = null;
					$mobile = null;
					$isOrg = null;
					while($row = $result->fetch_assoc()){
						$userId = $row["id"];
						$userName = $row["name"];
						$emailId = $row["email"];
						$mobile = $row["mobile"];
						$isOrg = $row["is_organization"];
						$userObject = FeedbackHelper::constructUserObject($userObject,$userId,$userName,$isOrg,$emailId,$mobile);
					}
					echo json_encode($userObject);
				}else{
					//http_response_code(500);					
					$errorObject = FeedbackHelper::constructErrorObject($errorObject,"NO_DATA_FOUND","No data found");
					error_log(json_encode($errorObject));
					echo json_encode($errorObject);
				}
				
			}
			
		}
	}
}catch(Exception $e){
	error_log($e);
	echo "Error occuered $e";	
}
}

if($getRequest){
		
try{
	if($getRequest["deleteUser"]){
		$errorObject = null;
		if($getRequest["email"]){
			//$errorObject = FeedbackHelper::constructErrorObject($errorObject,"QRY_NAME_EMPTY","Query name must not be empty");
			$email=$getRequest["email"];
		}else{
			$errorObject = FeedbackHelper::constructErrorObject($errorObject,"EMAIL_EMPTY","Email address is mandatory");
		}
		

		if($errorObject){
			//http_response_code(500);
			error_log(json_encode($errorObject));
			echo json_encode($errorObject);
		}else{
			
			$con = mysqli_connect("$dbserver","$dbuser","$dbpwd","$dbname");

			if(mysqli_connect_error()){
				error_log(mysqli_connect_error());
				$errorObject = FeedbackHelper::constructErrorObject($errorObject,"DB_CONNECT_ERROR","Could not connect to database");
				echo json_encode($errorObject);
				//http_response_code(500);
				//echo $errorObject;	
			}else{
				$query = "delete from `users` where email like 'test%' and email = '$email'";
				//delete from `users` where email like 'test%' and email = 'testvalidemail@domain.com'
				//echo("qry is $query");	
				$result = mysqli_query($con,$query);
				mysqli_commit($con);
				//echo($result);
				if($result){
					echo("success! User with email $email is deleted");
				}else{
					//http_response_code(500);
					$errMsg = mysqli_error($con);
					error_log($errMsg);
									
					$errorObject = FeedbackHelper::constructErrorObject($errorObject,"DB_OPERATION_ERROR","$errMsg");
					echo json_encode($errorObject);
				}
			}
			
		}
	}
}catch(Exception $e){
	error_log($e);
	echo "Error occuered $e";	
}
}
?>