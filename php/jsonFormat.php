<?php
include("helper.php");

//Errors Object
$errorJson = '{  
   "errors":[  
      {  
         "errorCode":"Error code 1",
         "errorDescription":"Error description 1"
      },
      {  
         "errorCode":"Error code 2",
         "errorDescription":"Error description 2"
      }
   ]
}';
//echo $errorJson;
$errorObject = null;
$errorObject = FeedbackHelper::constructErrorObject($errorObject,"My Error Code 3","My Error Description 3");
$errorObject = FeedbackHelper::constructErrorObject($errorObject,"My Error Code 4","My Error Description 4");
$errorObject = FeedbackHelper::constructErrorObject($errorObject,"My Error Code 5","My Error Description 5");
echo json_encode($errorObject);

//User object
$userJson = '{  
   "users":[  
      {  
         "userId":"1",
         "name":"Name 1",
         "email":"email 1",
         "isOrg":"is org 1"
      },
      {  
         "userId":"2",
         "name":"Name 2",
         "email":"email 2",
         "isOrg":"is org 2"
      }
   ]
}';

//echo $userJson;
$userObject = null;
$userObject = FeedbackHelper::constructUserObject($userObject,"1","Name 1",true,'sdasdas','23232');
$userObject = FeedbackHelper::constructUserObject($userObject,"2","Name 2",false,'sdasdas','23232');
$userObject = FeedbackHelper::constructUserObject($userObject,"3","Name 3",true,'sdasdas','23232');
echo json_encode($userObject);

//User products object
$productJson = '{  
   "products":[  
      {  
         "productId":"1",
         "name":"Product 1",
         "isService":"true",
         "spec":"{}",
         "category":"1",
      },
      {  
         "productId":"2",
         "name":"Product 2",
         "isService":"false",
         "spec":"{}",
         "category":"1",
      }
   ]
}';

//echo $userJson;
$productObject = null;
$productObject = FeedbackHelper::constructProductObject($productObject,"1","Prod Name 1",true,1,"{'spec1' : 'Spec 1'}");
$productObject = FeedbackHelper::constructProductObject($productObject,"2","Prod Name 2",false,1,"{'spec1' : 'Spec 1'}");
$productObject = FeedbackHelper::constructProductObject($productObject,"3","Prod Name 3",true,1,"{'spec1' : 'Spec 1'}");
echo json_encode($productObject);

//User std categ object
$feedbackCategJson = '{  
   "feedbackCategs":[  
      {  
         "name":"categ 1"
      },
      {  
         "name":"categ 2"
      }
   ]
}';

//echo $userJson;
$feedbackCategObject = null;
$feedbackCategObject = FeedbackHelper::constructFeedbackCategObject($feedbackCategObject,"Categ 1");
$feedbackCategObject = FeedbackHelper::constructFeedbackCategObject($feedbackCategObject,"Categ 2");
$feedbackCategObject = FeedbackHelper::constructFeedbackCategObject($feedbackCategObject,"Categ 3");
echo json_encode($feedbackCategObject);

//User std categ object
$seekDetailsJson = '{  
   "seekDetails":[  
      {  
         "seekerId":"1",
         "seekerName":"User name 1",
         "targetTypeId":"4",
         "targetTypeName":"ORGANIZATION",
         "productId":"1",
         "productName":"Product name 1",
         "feedbackCategories":[{"category":"Culture of Org"},{"category":"Goodwill of Org"}]
      },
      {  
         "seekerId":"2",
         "seekerName":"User name 2",
         "targetTypeId":"1",
         "targetTypeName":"PERSON",
         "productId":"2",
         "productName":"User name 1",
         "feedbackCategories":{"feedbackCategs":[{"name":"Culture of Org"},{"name":"Goodwill of Org"}]}
      }
   ]
}';

//echo $seekDetailsJson;
$constructSeekDetailObject = null;
$feedbackCategs = null;
$feedbackCategs = FeedbackHelper::constructFeedbackCategObject($feedbackCategs,"categ1");
$feedbackCategs = FeedbackHelper::constructFeedbackCategObject($feedbackCategs,"categ2");
$constructSeekDetailObject = FeedbackHelper::constructSeekDetailObject($constructSeekDetailObject,"1","Seeker Name1","1","Target type Name 1","1","Product Name 1",$feedbackCategs);
$constructSeekDetailObject = FeedbackHelper::constructSeekDetailObject($constructSeekDetailObject,"1","Seeker Name2","1","Target type Name 1","1","Product Name 1",$feedbackCategs);
$constructSeekDetailObject = FeedbackHelper::constructSeekDetailObject($constructSeekDetailObject,"1","Seeker Name3","1","Target type Name 1","1","Product Name 1",$feedbackCategs);
echo json_encode($constructSeekDetailObject);
?>