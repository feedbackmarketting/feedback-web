<?php
/**
	Helper class for various general purpose functionalities
*/
	require_once "random_compat.phar";

	class FeedbackHelper{

		static function getInitConfiguration(){

			$ini_array = parse_ini_file("../feedback.ini");
			//print_r($ini_array);
			return $ini_array;
		}

		static function constructErrorObject($errorObject,$errorCode,$errorDesc){
			if(is_null($errorObject)){
				$errorObject = array();
			}
			if(sizeof($errorObject) == 0){
				$errors = array();
				$errorObject["errors"] = $errors;
			}else{
				$errors = $errorObject["errors"]; 
			}
			//echo 'size of errors'.sizeof($errors);
			$error = array('errorCode' => $errorCode,'errorDescription' => $errorDesc);
			$errors[sizeof($errors)] = $error;
			$errorObject["errors"] = $errors;
			return $errorObject;
		}

		static function constructUserObject($userObject,$userId,$name,$isOrganization,$email,$mobile){
			if(is_null($userObject)){
				$userObject = array();
			}
			if(sizeof($userObject) == 0){
				$users = array();
				$userObject["users"] = $users;
			}else{
				$users = $userObject["users"]; 
			}
			//echo 'size of errors'.sizeof($errors);
			$user = array('userId' => $userId,'name' => $name,'isOrganization' => $isOrganization,'email' => $email,'mobile' => $mobile);
			//if($isOrganization){
			//	$user['isOrganization'] = $isOrganization;
			//}
			$users[sizeof($users)] = $user;
			$userObject["users"] = $users;
			return $userObject;
		}

		static function constructProductObject($productObject,$productId,$name,$isService,$categ,$spec){
			if(is_null($productObject)){
				$productObject = array();
			}
			if(sizeof($productObject) == 0){
				$products = array();
				$productObject["products"] = $products;
			}else{
				$products = $productObject["products"]; 
			}
			//echo 'size of errors'.sizeof($errors);
			$product = array('id' => $productId,'name' => $name,'isService' => $isService,'categ' => $categ,'spec' => $spec);
			
			$products[sizeof($products)] = $product;
			$productObject["products"] = $products;
			return $productObject;
		}

		static function constructFeedbackCategObject($feedbackCategObject,$name){
			if(is_null($feedbackCategObject)){
				$feedbackCategObject = array();
			}
			if(sizeof($feedbackCategObject) == 0){
				$feedbackCategs = array();
				$feedbackCategObject["feedbackCategs"] = $feedbackCategs;
			}else{
				$feedbackCategs = $feedbackCategObject["feedbackCategs"]; 
			}
			//echo 'size of errors'.sizeof($errors);
			$feedbackCateg = array('name' => $name);
			
			$feedbackCategs[sizeof($feedbackCategs)] = $feedbackCateg;
			$feedbackCategObject["feedbackCategs"] = $feedbackCategs;
			return $feedbackCategObject;
		}

		static function constructFeedbackCategRatingObject($feedbackCategObject,$name,$rating,$reason){
			if(is_null($feedbackCategObject)){
				$feedbackCategObject = array();
			}
			if(sizeof($feedbackCategObject) == 0){
				$feedbackCategs = array();
				$feedbackCategObject["feedbackCategs"] = $feedbackCategs;
			}else{
				$feedbackCategs = $feedbackCategObject["feedbackCategs"]; 
			}
			//echo 'size of errors'.sizeof($errors);
			$feedbackCateg = array('name' => $name,'rating' => $rating,'reason' => $reason);
			
			$feedbackCategs[sizeof($feedbackCategs)] = $feedbackCateg;
			$feedbackCategObject["feedbackCategs"] = $feedbackCategs;
			return $feedbackCategObject;
		}

		static function constructSeekDetailObject($seekDetailObject,$seekerId,$seekerName,$seekerEmail,$targetTypeId,$targetTypeName,$productId,$productName,$feedbackCategs){
			if(is_null($seekDetailObject)){
				$seekDetailObject = array();
			}
			if(sizeof($seekDetailObject) == 0){
				$seekDetails = array();
				$seekDetailObject["seekDetails"] = $seekDetails;
			}else{
				$seekDetails = $seekDetailObject["seekDetails"]; 
			}
			//echo 'size of errors'.sizeof($errors);
			$seekDet = array('seekerId' => $seekerId,'seekerName' => $seekerName,'seekerEmail' => $seekerEmail,'targetTypeId' => $targetTypeId,'targetTypeName' => $targetTypeName,'productId' => $productId,'productName' => $productName,'feedbackCategories' => $feedbackCategs);
			
			$seekDetails[sizeof($seekDetails)] = $seekDet;
			$seekDetailObject["seekDetails"] = $seekDetails;
			return $seekDetailObject;
		}

		static function constructFeedbackObject($feedbackObject,$feedbackId,$providerName,$providerEmail,$comment,$productName,$feedbackCategs,$feedbackDate){
			if(is_null($feedbackObject )){
				$feedbackObject  = array();
			}
			if(sizeof($feedbackObject ) == 0){
				$feedbacks = array();
				$feedbackObject ["feedbacks"] = $feedbacks;
			}else{
				$feedbacks = $feedbackObject ["feedbacks"]; 
			}
			//echo 'size of errors'.sizeof($errors);
			$feedback = array('feedbackId' => $feedbackId,'providerName' => $providerName,'providerEmail' => $providerEmail,'comment' => $comment,'productName' => $productName,'feedbackDate' => $feedbackDate,'feedbackCategories' => $feedbackCategs);
			
			$feedbacks[sizeof($feedbacks)] = $feedback;
			$feedbackObject ["feedbacks"] = $feedbacks;
			return $feedbackObject ;
		}

		static function sendEmail($emailTo,$subject,$body,$headers){
			//$emailTo="";	
			//$subject="I hope this works!";	
			//$body="I think you're great";	
			//$headers="From: rob@robpercival.co.uk";	
			if (mail($emailTo, $subject, $body, $headers)) {
				echo "Mail sent successfully!";
			}else{
				echo "Mail not sent!";	
			}

		}

		static function sendElasticEmail($to, $subject, $body_text, $body_html, $from, $fromName)
		{
		    $res = "";

		    $data = "username=".urlencode("<your_user_name_key>");
		    $data .= "&api_key=".urlencode("<your_api_key");
		    $data .= "&from=".urlencode($from);
		    $data .= "&from_name=".urlencode($fromName);
		    $data .= "&to=".urlencode($to);
		    $data .= "&subject=".urlencode($subject);
		    if($body_html)
		      //echo "$body_html";
		      $data .= "&body_html=".urlencode($body_html);
		    if($body_text)
		      $data .= "&body_text=".urlencode($body_text);

		    $header = "POST /mailer/send HTTP/1.0\r\n";
		    $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
		    $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
		    $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

		    if(!$fp)
		      return "ERROR. Could not open connection";
		    else {
		      fputs ($fp, $header.$data);
		      while (!feof($fp)) {
		        $res .= fread ($fp, 1024);
		      }
		      fclose($fp);
		    }
		    return $res;                  
		}

		static function generateRandomString($len){
			$bytes = random_bytes($len);
			return bin2hex($bytes);
		}
	}


?>