<?php
include("helper.php");
//echo("Users REST service");
$configs = FeedbackHelper::getInitConfiguration();
$baseUrl = $configs["baseUrl"];
$dbserver = $configs["dbserver"];
$dbname = $configs["dbname"];
$dbuser = $configs["dbuser"];
$dbpwd = $configs["dbpwd"];
$request = $_POST;

//print_r($request);
if($request){
		
try{
	if($request["accountSetup"]){

		$errorObject = null;
		$isProvider= null;
		$isOrg= null;
		if(!$request["userId"]){
			$errorObject = FeedbackHelper::constructErrorObject($errorObject,"USERID_EMPTY","User id must not be empty");			
		}else{
			$userId=$request["userId"];
		}
		if(!$request["isOrg"]){
			$errorObject = FeedbackHelper::constructErrorObject($errorObject,"IS_ORG_EMPTY","Is Organization must not be empty");	
		}else{
			$isOrg=$request["isOrg"];
		}
		echo "isOrg is $isOrg";
		/*if($isOrg == 'true'){
			if(!$request["isProvider"]){
				$errorObject = FeedbackHelper::constructErrorObject($errorObject,"IS_PROVIDER_EMPTY","Is Provider must not be empty");	
			}else{
				$isProvider=$request["isProvider"];
			}	
		}		
		if($isProvider){
			if(!$request["products"]){
				$errorObject = FeedbackHelper::constructErrorObject($errorObject,"PRODUCTS_EMPTY","Products/Services must not be empty");	
			}else{
				$products=$request["products"];
			}
		}*/

		if($errorObject){
			//http_response_code(500);
			error_log(json_encode($errorObject));
			echo json_encode($errorObject);
		}else{
			
			$con = mysqli_connect("$dbserver","$dbuser","$dbpwd","$dbname");

			if(mysqli_connect_error()){
				error_log(mysqli_connect_error());
				$errorObject = FeedbackHelper::constructErrorObject($errorObject,"DB_CONNECT_ERROR","Could not connect to database");
				echo json_encode($errorObject);
				//http_response_code(500);
				//echo $errorObject;	
			}else{
				$query = "UPDATE `users` SET `is_organization`=$isOrg where `id` = $userId";
				
				
				error_log("UPDATE qry is $query");	
				$result = mysqli_query($con,$query);
				if($result){
					if($isProvider){

					}	
					echo "Account setup successful";				
				}else{
					//http_response_code(500);					
					$errorObject = FeedbackHelper::constructErrorObject($errorObject,"DB_OPERATION_ERROR","Could not update database");
					error_log(json_encode($errorObject));
					echo json_encode($errorObject);
				}
				
			}
			
		}
	}
}catch(Exception $e){
	error_log($e);
	echo json_encode($e);	
}
}
?>