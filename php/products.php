<?php
include("helper.php");
//echo("Users REST service");
$configs = FeedbackHelper::getInitConfiguration();
$baseUrl = $configs["baseUrl"];
$dbserver = $configs["dbserver"];
$dbname = $configs["dbname"];
$dbuser = $configs["dbuser"];
$dbpwd = $configs["dbpwd"];
$request = $_GET;
$postRequest = $_POST;

//Getting the products
if($request){
		
try{
	if($request["getUserProducts"]){

		$errorObject = null;
		$productObject = null;
		$isService = null;
		if(!$request["user"]){
			$errorObject = FeedbackHelper::constructErrorObject($errorObject,"USER_EMPTY","User must not be empty");			
		}else{
			$user=$request["user"];
		}

		if($request["isService"] != null){
			$isService = $request["isService"];
		}		

		if($errorObject){
			//http_response_code(500);
			error_log(json_encode($errorObject));
			echo json_encode($errorObject);
		}else{
			$con = mysqli_connect("$dbserver","$dbuser","$dbpwd","$dbname");

			if(mysqli_connect_error()){
				error_log(mysqli_connect_error());
				$errorObject = FeedbackHelper::constructErrorObject($errorObject,"DB_CONNECT_ERROR","Could not connect to database");
				echo json_encode($errorObject);
				//http_response_code(500);
				//echo $errorObject;	
			}else{
				$query = "SELECT distinct p.`id`,p.`name`,p.`is_service`,p.`category`,
								p.`specification` FROM `user_products` upc, products p 
								where upc.`product` = p.`id` and upc.`user` = '$user' ";
				if($isService != null && $isService !=""){
					$query=$query."and is_service = '$isService' ";
				}				
				
				
				//echo("qry is $query");	
				$result = mysqli_query($con,$query);
				if($result->num_rows >0){
					$productId = null;
					$productName = null;
					$is_service = null;
					$categ = null;
					$spec = null;
					while($row = $result->fetch_assoc()){
						$productId = $row["id"];
						$productName = $row["name"];
						$is_service = $row["is_service"];

						$productObject = FeedbackHelper::constructProductObject($productObject,"$productId","$productName",$is_service,$categ,$spec);
					}
					echo json_encode($productObject);
				}else{
					//http_response_code(500);					
					$errorObject = FeedbackHelper::constructErrorObject($errorObject,"NO_DATA_FOUND","No data found");
					error_log(json_encode($errorObject));
					echo json_encode($errorObject);
				}
				
			}
			
		}
	}
}catch(Exception $e){
	error_log($e);
	echo json_encode($e);	
}
}

//Saving the products and cofigs
if($postRequest){

try{
	if($postRequest["configureProducts"]){
		$errorObject = null;
		$user = null;
		$products = null;
		if(!$postRequest["user"]){
			$errorObject = FeedbackHelper::constructErrorObject($errorObject,"USER_EMPTY","User must not be empty");			
		}else{
			$user=$postRequest["user"];
		}
		if(!$postRequest["products"]){
			$errorObject = FeedbackHelper::constructErrorObject($errorObject,"PRODUCTS_EMPTY","Products must not be empty");			
		}else{
			$products=$postRequest["products"];
		}

		if($errorObject){
			//http_response_code(500);
			error_log(json_encode($errorObject));
			echo json_encode($errorObject);
			die();
		}else{
			if($products){
				echo "Products are : ";
				print_r($products);
				$con = mysqli_connect("$dbserver","$dbuser","$dbpwd","$dbname");

				if(mysqli_connect_error()){
					error_log(mysqli_connect_error());
					$errorObject = FeedbackHelper::constructErrorObject($errorObject,"DB_CONNECT_ERROR","Could not connect to database");
					echo json_encode($errorObject);
				}else{
						$query = "";
						$productId = null;
						$prodName = null;
						$isService = null;
						$spec = null;
						$cat = null;
						foreach ($products as $prod){
							echo "Prod Name: ".$prod["name"];
							//echo "Is service: $prod['isService']";
							echo "<br/>";
							$productId = null;
							$productId = $prod["id"];
							$prodName = $prod["name"];
							$isService = $prod["isService"];
							//$prodName = $prod["name"];
							//$prodName = $prod["name"];
							if(!$productId){
								$query = "INSERT INTO `products` (`name`,`is_service`,`created_date`) ";
								$query = $query."VALUES ('$prodName',$isService,sysdate())";	
								
								echo("qry is $query");	
								$result = mysqli_query($con,$query);
								echo("result is : ".$result);
								if($result){
									
									$query = "SELECT p.`id` FROM `products` p where p.`name` = '$prodName'";
									//echo("qry is $query");	
									$result_sel = mysqli_query($con,$query);
									if($result_sel->num_rows >0){
										while($row = $result_sel->fetch_assoc()){
											$productId = $row["id"];
										}
									}

									$query = "INSERT INTO `user_products` (`user`,`product`,`created_date`) ";
									$query = $query."VALUES ('$user',$productId,sysdate())";
									echo("qry is $query");	
									$result1 = mysqli_query($con,$query);
									if($result1){
										echo("Success! Products configured successfully");
									}else{
										mysqli_rollback($con);
										$errorObject = FeedbackHelper::constructErrorObject($errorObject,"DB_OPERATION_ERROR","Could not insert into database");
										echo json_encode($errorObject);
									}								
								}else{
									//http_response_code(500);					
									$errorObject = FeedbackHelper::constructErrorObject($errorObject,"DB_OPERATION_ERROR","Could not insert into database");
									echo json_encode($errorObject);

								}
							}
								
						}
						
					
					}
			}
		}
		
	}
}catch(Exception $e){
	error_log($e);
	$errorObject = FeedbackHelper::constructErrorObject($errorObject,"GENERAL_ERROR","General Error");
	echo json_encode($errorObject);	
}

}

?>