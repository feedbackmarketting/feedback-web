(function(){
    
    var ProductService = function($http,$httpParamSerializerJQLike){
        
        //$httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';        
        this.getUserProducts = function(userId,isService){
            //return $http.post('php/users.php',$httpParamSerializerJQLike(user));
            return $http({method:'GET', url:'php/products.php?getUserProducts=yes&user='+userId+'&isService='+isService, data:$httpParamSerializerJQLike({}), crypt:false, logging:false});
        };
        
        this.configureProducts = function(userId,productsList){
            //console.log("product list in service : "+productsList);
            var parameter = {};
            parameter.user = userId;
            parameter.configureProducts = "yes";
            parameter.products = productsList;
            //console.log("serialized string in product config : "+$httpParamSerializerJQLike(parameter));
            return $http({method:'POST', url:'php/products.php', data:$httpParamSerializerJQLike(parameter), crypt:false, logging:false});
        };
        
        this.getStdCategs = function(target){
            //return $http.post('php/users.php',$httpParamSerializerJQLike(user));
            return $http({method:'GET', url:'php/feedbackCategories.php?getStdCategs=yes&target='+target, data:$httpParamSerializerJQLike({}), crypt:false, logging:false});
        };
    };
    
    ProductService.$inject = ['$http','$httpParamSerializerJQLike'];
    
    angular.module('feedbackApp').service('ProductService',ProductService);
}());