(function(){
    
    var FeedbackService = function($http,$httpParamSerializerJQLike){

        this.seekFeedback = function(userId,targetCateg,audience,categories,productId,userName,productName){
            //console.log("Seek feedback service called ");
            var parameter = {};
            parameter.user = userId;
            parameter.seek = "yes";
            parameter.targetCateg = targetCateg;
            parameter.audience = audience;
            parameter.categories = categories;
            parameter.product = productId;
            parameter.userName = userName;
            parameter.productName = productName;
            return $http({method:'POST', url:'php/feedback.php', data:$httpParamSerializerJQLike(parameter), crypt:false, logging:false});
        };
        
        this.fetchSeekDetails = function(seekId,targetTypeId){
            return $http({method:'GET', url:'php/feedback.php?getSeekDetails=yes&seekId='+seekId+'&targetTypeId='+targetTypeId, data:$httpParamSerializerJQLike({}), crypt:false, logging:false});
        };
        
        this.provideFeedback = function(providerId,providerEmail,targetCateg,
                    targetId,categories,overallComment,seekId,userName,productName,seekerEmail){
            //console.log("provide feedback service called ");
            var parameter = {};
            parameter.provide = "yes";
            parameter.user = providerId; // optional for sought feedback but mandatory for logged in user
            parameter.providerEmail = providerEmail;//mandatory
            parameter.targetCateg = targetCateg;//mandatory
            parameter.categories = categories;//mandatory
            parameter.product = targetId;//mandatory
            parameter.overallComment = overallComment;//optional
            parameter.seekId = seekId;//optional but mandatory for sought feedback
            parameter.userName = userName;//optional but mandatory for sought feedback
            parameter.productName = productName;//optional but mandatory for sought feedback
            parameter.seekerEmail = seekerEmail;//optional but mandatory for sought feedback
            return $http({method:'POST', url:'php/feedback.php', data:$httpParamSerializerJQLike(parameter), crypt:false, logging:false});
        };
        
        this.fetchFeedbacks = function(userId,targetTypeId){
            return $http({method:'GET', url:'php/feedback.php?getFeedbacks=yes&userId='+userId+'&targetTypeId='+targetTypeId, data:$httpParamSerializerJQLike({}), crypt:false, logging:false});
        };

        this.searchOrganisation = function(name){
            return $http({method:'GET', url:'php/users.php?qryUser=yes&isOrg=1&name='+name, data:$httpParamSerializerJQLike({}), crypt:false, logging:false});
        };
    };
    
    FeedbackService.$inject = ['$http','$httpParamSerializerJQLike'];
    
    angular.module('feedbackApp').service('FeedbackService',FeedbackService);
}());