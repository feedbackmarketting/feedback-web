(function(){
    
    var navigationFactory = function(){
        var appName = "ShowMyFeedback.com";
        
        var navigationTabs = {home:{active:'active'},register:{active:''},login:{active:''},logout:{active:''},userHome:{active:''},seek:{active:''},provide:{active:''}};
        
        var factory ={};
        
        factory.getAppName = function(){
          return appName;  
        };
        
        factory.getNavigationTabs = function(){
          return navigationTabs;  
        };
        
        var loggedInUser = {};
        factory.getLoggedInUser = function(){
            //console.log("loggedinuser in nav factory "+loggedInUser);
            return loggedInUser;
        }
        
        factory.setLoggedInUser = function(user){
             console.log(JSON.stringify(user));
             loggedInUser.userId = user.userId;
             loggedInUser.name = user.name;
             loggedInUser.isOrganization =user.isOrganization;
            //loggedInUser = data;
        }
        
        var accountSetup = {};
        factory.getAccountSetup = function(){
            return accountSetup;
        }
        
        factory.setAccountSetup = function(data){
            accountSetup.isOrg = data.isOrg;
            accountSetup.isProvider = data.isProvider;
        }
        
        return factory;
    };
    
    angular.module('feedbackApp').factory('navigationFactory',navigationFactory);
}());