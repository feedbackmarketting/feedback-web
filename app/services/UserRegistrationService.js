(function(){
    
    var UserRegistrationService = function($http,$httpParamSerializerJQLike){
        
        //$httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';        
        this.register = function(user){
            
           //console.log("user object in register : "+$httpParamSerializerJQLike(user));
            return $http({method:'POST', url:'php/users.php', data:$httpParamSerializerJQLike(user), crypt:true, logging:false});
        };
        
        this.login = function(loggedInUser){
            return $http({method:'POST', url:'php/login.php', data:$httpParamSerializerJQLike(loggedInUser), crypt:true, logging:false});
        };
        
        this.setupAccount = function(loggedInUser,accountSetup){
            var parameters = {};
            parameters.accountSetup="accountSetup";
            parameters.userId = loggedInUser.userId;
            parameters.isOrg = accountSetup.isOrg;
            parameters.isProvider = accountSetup.isProvider;
            parameters.products = accountSetup.products;
            return $http({method:'POST', url:'php/accountSetup.php', data:$httpParamSerializerJQLike(parameters), crypt:false, logging:false});
        };
        
        this.searchUser = function(email,userId){
            return $http({method:'GET', url:'php/users.php?qryUser=yes&email='+email+'&userId='+userId, data:$httpParamSerializerJQLike({}), crypt:false, logging:false});
        };
        
        this.sendPasswordLink = function(email){
            var parameter = {};
            parameter.sendPasswordLink="yes";
            parameter.email = email;
            return $http({method:'POST', url:'php/login.php', data:$httpParamSerializerJQLike(parameter), crypt:false, logging:false});
        };
        
        this.resetPassword = function(resetForm){
            resetForm.resetPassword = "yes";
            return $http({method:'POST', url:'php/login.php', data:$httpParamSerializerJQLike(resetForm), crypt:true, logging:false});
        };
    };
    
    UserRegistrationService.$inject = ['$http','$httpParamSerializerJQLike'];
    
    angular.module('feedbackApp').service('UserRegistrationService',UserRegistrationService);
}());