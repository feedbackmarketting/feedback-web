    (function(){

        var FeedbackMeRatingDirective = function(){

            var maxRating = 5;
            var currentRating = 0;
            var readOnly = false;

            return {               
                scope : {
                    feedbackcateg:"=feedbackcateg",
                    readonly:"=readonly"
                },
                templateUrl : "app/views/rating.html",
                link : function (scope,element,attrs){
                    currentRating = 0;
                    readOnly = false;
                    if(scope.feedbackcateg && scope.feedbackcateg.rating){
                        currentRating = scope.feedbackcateg.rating;    
                    }                    
                    //console.log("curent rating"+currentRating);
                    if(scope.readonly){
                        readOnly = scope.readonly;    
                    }
                    //console.log("rating readonly"+readOnly);
                    
                    var directiveElement = element[0]; 
                    var ulElement = directiveElement.firstElementChild;
                    var liElements = ulElement.children;
                    //console.log(JSON.stringify(ulElement));
                    setRating(parseInt(currentRating),liElements);
                    
                    scope.starClick = function(ratingIndex){
                        if(readOnly!==true){
                            setRating(parseInt(ratingIndex) + 1,liElements);    
                        }                        
                    };

                    function iterate(collection, callback) {
                      for (var i = 0; i < collection.length; i++){
                        var item = collection[i];
                         // console.log("item is "+item);
                        callback(item, i);
                      }
                    }

                    function setRating(value,children) {
                      if (value && value < 0 || value > maxRating) { return; }

                      currentRating = value || currentRating;
                      scope.feedbackcateg.rating=currentRating;
                     // console.log(JSON.stringify($(children.context)));
                      var stars = children;    

                      iterate(stars, function(star, index) {
                          //console.log("index is "+index);
                        if (index < currentRating) {
                            if(currentRating > 3){
                                star.classList.remove('c-rating__item-red');
                                star.classList.remove('c-rating__item');
                                star.classList.add('c-rating__item-green');
                            }else if(currentRating < 3){
                                star.classList.remove('c-rating__item-green');
                                star.classList.remove('c-rating__item');
                                star.classList.add('c-rating__item-red');
                            }else{
                                star.classList.remove('c-rating__item-green');
                                star.classList.remove('c-rating__item-red');
                                star.classList.add('c-rating__item');
                            }
                          star.classList.add('is-active');
                        } else {
                          star.classList.remove('is-active');
                          if(currentRating > 3){
                                star.classList.remove('c-rating__item-red');
                                star.classList.remove('c-rating__item');
                                star.classList.add('c-rating__item-green');      
                            }else if(currentRating < 3){
                                star.classList.remove('c-rating__item-green');
                                star.classList.remove('c-rating__item');
                                star.classList.add('c-rating__item-red');
                            }else{
                                star.classList.remove('c-rating__item-green');
                                star.classList.remove('c-rating__item-red');
                                star.classList.add('c-rating__item');
                            }   
                        }
                      });

                       //console.log("New rating is : "+JSON.stringify(scope.feedbackcateg));
                    }
                }
            };
        };

        angular.module('feedbackApp').directive('feedbackmeRating',FeedbackMeRatingDirective);
    }());