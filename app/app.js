(function(window){

    /**Global object for the feedbackMe application.
     * Attached to the window object
     */
    window.feedbackMe = window.feedbackMe || {};

    /**Some common utilities */

    /**
     * @name feedbackMe.setTabCss     * 
     * @description
     * Set the css classes for the menu tabs based on navigation context
     * @param {$scope.tabs} The entire tab object.
     * @param {Comma separated string} The names of the active tabs as comma separated string
     * @param {Comma separated string} The names of the inactive tabs as comma separated string
     * @param {Comma separated string} The names of the hidden tabs as comma separated string  
     */
    feedbackMe.setTabCss = function(tabs,activeTabs,inActiveTabs,hiddenTabs){
            setTabCssProperty(activeTabs,"active");
            setTabCssProperty(inActiveTabs,"");
            setTabCssProperty(hiddenTabs,"hidden");
            
            function setTabCssProperty(list,cssClass){
               if(typeof list === "string" && list !== ""){
                   list.split(",").forEach(function(item){
                    tabs[item].active = cssClass;    
                });
               }
            }
        };
    /**End of some common utilities */

    var RouteConfig = function($routeProvider){
        $routeProvider
            .when('/', {
                controller:'HomeController',
                templateUrl:'app/views/home.html'
            })
            .when('/register',{
                controller:'UserRegistrationController',
                templateUrl:'app/views/register.html'
            })
            .when('/registerSuccess',{
                controller:'HomeController',
                templateUrl:'app/views/registerSuccess.html'
            }).when('/login',{
                controller:'LoginController',
                templateUrl:'app/views/login.html'
            }).when('/login/:source',{
                controller:'LoginController',
                templateUrl:'app/views/login.html'
            }).when('/userHome',{
                controller:'UserHomeController',
                templateUrl:'app/views/userHome.html'
            }).when('/accountSetup',{
                controller:'AccountSetupController',
                templateUrl:'app/views/accountSetup.html'
            }).when('/accountSetup/:source',{
                controller:'AccountSetupController',
                templateUrl:'app/views/accountSetup.html'
            }).when('/productConfig',{
                controller:'ProductConfigController',
                templateUrl:'app/views/productConfig.html'
            }).when('/seekFeedback',{
                controller:'SeekFeedbackController',
                templateUrl:'app/views/seekFeedback.html'
            }).when('/provideVoluntaryFeedback',{
                controller:'ProvideVoluntaryFeedbackController',
                templateUrl:'app/views/provideVoluntaryFeedback.html'
            }).when('/provideFeedback/:provider/:seekId/:targetTypeId',{
                controller:'ProvideFeedbackController',
                templateUrl:'app/views/provideFeedback.html'
            }).when('/provideFeedbackSuccess',{
                templateUrl:'app/views/provideFeedbackSuccess.html'
            }).when('/sendPasswordLink',{
                controller:'LoginController',
                templateUrl:'app/views/sendPasswordLink.html'
            }).when('/resetPassword/:resetEmail/:activationLink',{
                controller:'LoginController',
                templateUrl:'app/views/resetPassword.html'
            }).when('/terms',{
                templateUrl:'app/views/terms.html'
            }).when('/learnMore',{
                templateUrl:'app/views/learnMore.html'
            })
            .otherwise({redirectTo:'/'});
    };
    RouteConfig.$inject = ['$routeProvider'];
   
    var feedbackApp = angular.module('feedbackApp',['ngRoute','angularjs-crypto','ngTagsInput'],['$httpProvider',function($httpProvider){
        $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
        //$httpProvider.defaults.paramSerializer = '$httpParamSerializerJQLike';        
    }]);
    feedbackApp.config(RouteConfig);    
    feedbackApp.run(['cfCryptoHttpInterceptor', function(cfCryptoHttpInterceptor) {
    cfCryptoHttpInterceptor.base64Key = function(){
        var key = "77ca2a3123927afd622781653ea5e967c51fa5122551b8a9a8f8a7cc1e407d87";
        var salt1 = "kethaz";
        var salt2 = "adw";
        var salt3 = "pavalam";
        key = key.replace("af",salt3);
        key = key.replace("fa",salt2);
        key = key.replace("1e",salt1);
        return key;
    }();
    cfCryptoHttpInterceptor.logging = false;
}])
}(window));
