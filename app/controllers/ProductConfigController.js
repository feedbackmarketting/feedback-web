(function () {

    var ProductConfigController = function ($scope, navigationFactory, $routeParams, ProductService) {
        
        function init() {
            $scope.tabs = navigationFactory.getNavigationTabs();
            //console.log('tabs : '+JSON.stringify($scope.tabs));
            
            feedbackMe.setTabCss($scope.tabs,"","logout","home,register,login,provide");
            
            if (!navigationFactory.getLoggedInUser().userId) {
                location.href = "#/login";
                return;
            }
            $scope.userLoggedIn = navigationFactory.getLoggedInUser();
            //console.log('userLoggedIn : '+JSON.stringify($scope.userLoggedIn));
            
            //$scope.stdProdCategs = [{id: 6, name: "Price", target: 2}, {id: 7, name: "Quality", target: 2}, {id: 9, name: "Warrenty", target: 2}];
            //$scope.stdServCategs = [{id: 11, name: "Promptness service", target: 3}, {id: 12, name: "Quality of service", target: 3}, {id: 13, name: "Friendlyness of the service provider", target: 3}];
            $scope.stdCategs = [];
            
            $scope.action = "configProducts";
            
            $scope.products = [];
            //$scope.products = [{id: "", name: "", isService: "", categories: $scope.stdCategs}];
            
            ProductService.getUserProducts(navigationFactory.getLoggedInUser().userId).success(function(data){
                //console.log("Login successful : "+JSON.stringify(data));
                var resString = JSON.stringify(data);
                if(resString.indexOf("errors") > 0 ){
                    if(resString.indexOf("NO_DATA_FOUND") > 0){
                        $scope.products = [];
                    }else{
                        $scope.errorObject = showErrors(JSON.stringify(data));
                        $("#errorConfigAlert").show();
                        return;    
                    }
                }else{                            
                    $scope.products = extractProducts(data);                                                      
                }                    
            //return false;
            }).error(function(data){
                //console.log("Registration failure"); 
                $scope.errorObject = showErrors(JSON.stringify(data));
                $("#errorConfigAlert").show();
            });
            
            //console.log(JSON.stringify($scope.products));    
            
            
            $scope.accountType = "";
            //$scope.accountType.desc = "Select";
            
          
        }
        init();    
        
        $scope.selectProdType = function(prod){
            
            //console.log("prod is "+prod);
            if(prod.isService === 'true'){
                //console.log("it is service")
                prod.categories = $scope.stdServCategs;
                prod.target = 3;
            }else{
                prod.categories = $scope.stdProdCategs;
                prod.target = 2;
            }
        }
        
        $scope.addCategory = function(prod){
            
            var len = prod.categories.length;
            prod.categories[len] = {id: "", name: "", target: prod.target, isNew: true};
        }
        
        $scope.removeCateg = function(prod,catg){
            var indx = prod.categories.indexOf(catg);
            prod.categories.splice(indx,1);
        }
        
        $scope.addProduct = function(){
            var len = $scope.products.length;
            $scope.products[len] = {id: "", name: "", isService: "", categories: $scope.stdCategs};
        }
        
        $scope.removeProduct = function(prod){
            var indx = $scope.products.indexOf(prod);
            $scope.products.splice(indx,1);
        }
        
        $scope.configProducts = function(){
            var config = {};
            config.user = navigationFactory.getLoggedInUser().userId;
            config.isProvider = $scope.isProvider;
            
                //console.log("config form valid");
                ProductService.configureProducts(navigationFactory.getLoggedInUser().userId,$scope.products).success(function(data){
                    //console.log("Login successful : "+JSON.stringify(data));
                    var resString = JSON.stringify(data);
                    if(resString.indexOf("errors") > 0 ){                        
                        $scope.errorObject = showErrors(JSON.stringify(data));
                        $("#errorConfigAlert").show();
                        return;
                    }else{                            
                        location.href = "#/userHome";                                                            
                    }                    
                //return false;
                }).error(function(data){
                    //console.log("Registration failure"); 
                    $scope.errorObject = showErrors(JSON.stringify(data));
                    $("#errorConfigAlert").show();
                });
            
        }
        
        function showErrors(errorObject){
            var jsonObj = JSON.parse(errorObject);
            var errString = "";
            if(jsonObj){
                var errorArray = jsonObj.errors;
            //console.log("errrror array is : "+JSON.stringify(errorArray));
            
            for(var obj in errorArray){
                //console.log("errrror object is : "+JSON.stringify(obj));
                errString = errString+errorArray[obj]["errorDescription"]+" , ";
            }
            errString = errString.substr(0,errString.length-2);
            }
            return errString;
        }
        
        function extractProducts(data){ 
            if(data){
                var dataArray = data.products;
                //console.log("data array is : "+JSON.stringify(dataArray));
                return dataArray;
            }
        }
               
        
    };
    
    ProductConfigController.$inject = ['$scope','navigationFactory','$routeParams','ProductService'];    
   
    angular.module('feedbackApp').controller('ProductConfigController',ProductConfigController);
}());