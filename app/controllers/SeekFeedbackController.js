(function () {

    var SeekFeedbackController = function ($scope, navigationFactory, $routeParams, ProductService,UserRegistrationService,FeedbackService) {
        
        function init() {
            $scope.tabs = navigationFactory.getNavigationTabs();
           
            feedbackMe.setTabCss($scope.tabs,"seek","logout,userHome,provide","home,register,login");
            
            if (!navigationFactory.getLoggedInUser().userId) {
                location.href = "#/login";
                return;
            }
            $scope.userLoggedIn = navigationFactory.getLoggedInUser();
            $scope.action = "seekFeedback";
            JSON.stringify("logged in user "+$scope.userLoggedIn);
            var personalFeedback = {};
            $scope.targetCateg = "";
            if($scope.userLoggedIn.isOrganization === "1"){
                personalFeedback = {value:"org",description:"Seek feedback on my oraganization"}
                $scope.targetCateg = "ORGANIZATION";
                getStdCategs($scope.targetCateg);
            }else{
                personalFeedback = {value:"person",description:"Seek feedback on me"}   
                $scope.targetCateg = "PERSON";
                getStdCategs($scope.targetCateg);
            }
            
            $scope.seekFeedbackForm = {};
            $scope.seekFeedbackForm.target = "";
            $scope.seekFeedbackForm.targetOptions = [];
            $scope.seekFeedbackForm.targetOptions[0] = personalFeedback;
            if($scope.userLoggedIn.isOrganization === "1"){
                $scope.seekFeedbackForm.targetOptions[1] = {value:"product",description:"Seek feedback on my product / service"};
            }
            //$scope.seekFeedbackForm.targetOptions = [personalFeedback,{value:"product",description:"Seek feedback on my product / service"}];
            $scope.isProduct = false;
            ProductService.getUserProducts(navigationFactory.getLoggedInUser().userId).success(function(data){
                var resString = JSON.stringify(data);
                if(resString.indexOf("errors") > 0 ){
                    if(resString.indexOf("NO_DATA_FOUND") > 0){
                        $scope.seekFeedbackForm.products = [];
                    }else{
                        $scope.errorObject = showErrors(JSON.stringify(data));
                        $("#errorSeekAlert").show();
                        return;    
                    }
                }else{                            
                    $scope.seekFeedbackForm.products = extractProducts(data);                                                      
                }                    
            //return false;
            }).error(function(data){
                //console.log("Registration failure"); 
                $scope.errorObject = showErrors(JSON.stringify(data));
                $("#errorSeekAlert").show();
            });
            $scope.seekFeedbackForm.product = "";
            $scope.seekFeedbackForm.productSelected = {};
            
            //$scope.seekFeedbackForm.products = [{id:1,name:"Product 1"},{id:2,name:"Service 1"}];
            $scope.seekFeedbackForm.audience = [];
            $scope.stdCategs = [];
            //$scope.stdCategs = [{id: 6, name: "Price", target: 2}, {id: 7, name: "Quality", target: 2}, {id: 9, name: "Warrenty", target: 2}];
            
            
            $scope.seekFeedbackForm.users = [];
            UserRegistrationService.searchUser("%",navigationFactory.getLoggedInUser().userId).success(function(data){
                var resString = JSON.stringify(data);
                if(resString.indexOf("errors") > 0 ){
                    if(resString.indexOf("NO_DATA_FOUND") > 0){
                        $scope.seekFeedbackForm.users = [];
                    }else{
                        $scope.errorObject = showErrors(JSON.stringify(data));
                        $("#errorSeekAlert").show();
                        return;    
                    }
                }else{                            
                    $scope.seekFeedbackForm.users = extractUsers(data);            
                }                    
            //return false;
            }).error(function(data){
                //console.log("Registration failure"); 
                $scope.errorObject = showErrors(JSON.stringify(data));
                $("#errorSeekAlert").show();
            });
            
        };
        init();
        
        $scope.selectTarget = function(){
            $scope.seekFeedbackForm.product = "";
            $scope.seekFeedbackForm.productSelected = {};
            //console.log("target is "+$scope.seekFeedbackForm.target);
            if($scope.seekFeedbackForm.target === 'product'){
                //console.log("it is for product")
                $scope.isProduct = true;
            }else{
                $scope.isProduct = false;
                if($scope.userLoggedIn.isOrganization === "1"){
                    personalFeedback = {value:"org",description:"Seek feedback on my oraganization"}
                    $scope.targetCateg = "ORGANIZATION";
                    getStdCategs($scope.targetCateg);
                }else{
                    personalFeedback = {value:"person",description:"Seek feedback on me"}   
                    $scope.targetCateg = "PERSON";
                    getStdCategs($scope.targetCateg);
                }
            }
        }
        
        $scope.selectProduct = function(){
            $scope.seekFeedbackForm.productSelected = JSON.parse($scope.seekFeedbackForm.product);
            //console.log("product is "+$scope.seekFeedbackForm.productSelected + "$$$$$$$"+(isService($scope.seekFeedbackForm.productSelected)));
            if(isService($scope.seekFeedbackForm.productSelected)){
                //console.log("it is service")
                $scope.targetCateg = "SERVICE";
                getStdCategs($scope.targetCateg);
            }else{
                $scope.targetCateg = "PRODUCT";
                getStdCategs($scope.targetCateg);
            }
        }
        
        $scope.loadUsers = function(query) {     
            var emailArr = [];
            for(var i in $scope.seekFeedbackForm.users){
                emailArr[emailArr.length] = $scope.seekFeedbackForm.users[i].email;
            }
            return emailArr;
        };
        
        $scope.addCategory = function(){
            //console.log("add categ");
            var len = $scope.stdCategs.length; 
            $scope.stdCategs[len] = {id: "", name: "", target: 2, isNew: true};
        }
        
        $scope.removeCateg = function(catg){
            //console.log("remove categ");
            var indx = $scope.stdCategs.indexOf(catg);
            $scope.stdCategs.splice(indx,1);
        }
                
        $scope.seekFeedback = function(){
            var config = {};
            config.user = navigationFactory.getLoggedInUser().userId;

            //console.log("seekfeedback form valid");
            FeedbackService.seekFeedback(navigationFactory.getLoggedInUser().userId,$scope.targetCateg,$scope.seekFeedbackForm.audience,$scope.stdCategs,$scope.seekFeedbackForm.productSelected.id,navigationFactory.getLoggedInUser().name,$scope.seekFeedbackForm.productSelected.name).success(function(data){
                //console.log("Login successful : "+JSON.stringify(data));
                var resString = JSON.stringify(data);
                if(resString.indexOf("errors") > 0 ){                        
                    $scope.errorObject = showErrors(JSON.stringify(data));
                    $("#errorSeekAlert").show();
                    return;
                }else{      
                    $("#successSeekAlert").show();
                    //location.href = "#/userHome";                                                            
                }                    
            //return false;
            }).error(function(data){
                //console.log("Registration failure"); 
                $scope.errorObject = showErrors(JSON.stringify(data));
                $("#errorSeekAlert").show();
            });
            
        }
        
        function isService(product){
            for (var prod in $scope.seekFeedbackForm.products){
                //console.log("prod id : "+$scope.seekFeedbackForm.products[prod].id+" ££££ pId :"+productId)
                if($scope.seekFeedbackForm.products[prod].id === product.id){
                    if($scope.seekFeedbackForm.products[prod].isService ==="1"){
                        return true;
                    }else{
                        return false;
                    }
                }
            }
            return false;
        }
        
        function getStdCategs(target){
            ProductService.getStdCategs(target).success(function(data){
                var resString = JSON.stringify(data);
                if(resString.indexOf("errors") > 0 ){
                    if(resString.indexOf("NO_DATA_FOUND") > 0){
                        $scope.seekFeedbackForm.products = [];
                    }else{
                        $scope.errorObject = showErrors(JSON.stringify(data));
                        $("#errorSeekAlert").show();
                        return;    
                    }
                }else{                            
                    $scope.stdCategs = extractCategs(data);                                                      
                }                    
            //return false;
            }).error(function(data){
                //console.log("Registration failure"); 
                $scope.errorObject = showErrors(JSON.stringify(data));
                $("#errorSeekAlert").show();
            });
        }
        
        function showErrors(errorObject){
            var jsonObj = JSON.parse(errorObject);
            var errString = "";
            if(jsonObj){
                var errorArray = jsonObj.errors;
            //console.log("errrror array is : "+JSON.stringify(errorArray));
            
            for(var obj in errorArray){
                //console.log("errrror object is : "+JSON.stringify(obj));
                errString = errString+errorArray[obj]["errorDescription"]+" , ";
            }
            errString = errString.substr(0,errString.length-2);
            }
            return errString;
        }
        
        function extractProducts(data){ 
            if(data){
                var dataArray = data.products;
                //console.log("data array is : "+JSON.stringify(dataArray));
                return dataArray;
            }
        }
        
        function extractCategs(data){ 
            if(data){
                var dataArray = data.feedbackCategs;
                //console.log("data array is : "+JSON.stringify(dataArray));
                return dataArray;
            }
        }
        
        function extractUsers(data){ 
            if(data){
                var dataArray = data.users;
                //console.log("data array is : "+JSON.stringify(dataArray));
                return dataArray;
            }
        }
    };
    
    SeekFeedbackController.$inject = ['$scope','navigationFactory','$routeParams','ProductService','UserRegistrationService','FeedbackService'];    
   
    angular.module('feedbackApp').controller('SeekFeedbackController',SeekFeedbackController);
}());