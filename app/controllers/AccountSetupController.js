(function(){

    var AccountSetupController = function($scope,navigationFactory,$routeParams,UserRegistrationService){
        
        function init(){
            $scope.tabs=navigationFactory.getNavigationTabs();
            //console.log('tabs : '+JSON.stringify($scope.tabs));            
            feedbackMe.setTabCss($scope.tabs,"","logout","home,register,login,provide");
            
            if($routeParams.source === "test"){
                //console.log("inside test");
                navigationFactory.setLoggedInUser({userId:"11",name:'Automation User',isOrganization:""});                
            }

            //console.log("in account setup"+JSON.stringify(navigationFactory.getLoggedInUser()));

            if(!navigationFactory.getLoggedInUser().userId){
                location.href="#/login";
                return;
            }
            $scope.userLoggedIn = navigationFactory.getLoggedInUser();
            //console.log('userLoggedIn : '+JSON.stringify($scope.userLoggedIn));
            
            $scope.action = "accountSetup";
            $scope.accountType = "";
            //$scope.accountType.desc = "Select";
            
            $scope.isProvider = "";
            //$scope.isProvider.desc = "Select";
            
            $scope.isOrg = false;
        }
        init();    
        
        $scope.selectAccountType = function(){
            var typ = $scope.accountType;
            //console.log("acc type selected is "+typ);
            if(typ === 'organization'){
               $scope.isOrg = true;
               //$scope.accountType.name = "organization";
               //$scope.accountType.desc = "This account is for my organization"; 
            }else{
                $scope.isOrg = false;
                $scope.isProvider = "";
                //$scope.accountType.name = "personal";
                //$scope.accountType.desc = "This account is for my personal use"; 
                //$scope.isProvider = {};
                //$scope.isProvider.desc = "Select";
            }
        }
        
        $scope.selectIsProvider = function(ele){
            $scope.isProvider = ele;
            //console.log("provider selected is "+JSON.stringify(ele));
        }
        
        $scope.accountSetup = function(){
            var accountSetup = {};
            accountSetup.isOrg = $scope.isOrg;
            accountSetup.isProvider = $scope.isProvider;
        
            //Set the is organization into navigation factory
            if($scope.isOrg === true){
                $scope.userLoggedIn.isOrganization = "1";
                $scope.userLoggedIn.isProvider = $scope.isProvider;    
            }else{
                $scope.userLoggedIn.isOrganization  = "0";
            }
            navigationFactory.setLoggedInUser($scope.userLoggedIn);
            
            if($scope.setupForm.$valid){
                navigationFactory.setAccountSetup(accountSetup);
                UserRegistrationService.setupAccount($scope.userLoggedIn,accountSetup).success(function(data){
                    //console.log("Login successful : "+JSON.stringify(data));
                    var resString = JSON.stringify(data);
                    if(resString.indexOf("errors") > 0 ){                        
                        $scope.errorObject = showErrors(JSON.stringify(data));
                        $("#errorSetupAlert").show();
                        return;
                    }else{
                        if($scope.isOrg === false || $scope.isProvider === 'false'){
                            location.href = "#/userHome";
                        }else{
                            location.href = "#/productConfig";            
                        }
                    }                    
                //return false;
                }).error(function(data){
                    //console.log("Registration failure"); 
                    $scope.errorObject = showErrors(JSON.stringify(data));
                    $("#errorSetupAlert").show();
                });
            }
            
        }
        
        function showErrors(errorObject){
            var jsonObj = JSON.parse(errorObject);
            var errString = "";
            if(jsonObj){
                var errorArray = jsonObj.errors;
            //console.log("errrror array is : "+JSON.stringify(errorArray));
            
            for(var obj in errorArray){
                //console.log("errrror object is : "+JSON.stringify(obj));
                errString = errString+errorArray[obj]["errorDescription"]+" , ";
            }
            errString = errString.substr(0,errString.length-2);
            }
            return errString;
        }
               
        
    };
    
    AccountSetupController.$inject = ['$scope','navigationFactory','$routeParams','UserRegistrationService'];    
   
    angular.module('feedbackApp').controller('AccountSetupController',AccountSetupController);
}());