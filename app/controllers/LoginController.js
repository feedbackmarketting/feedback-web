(function(){

    var LoginController = function($scope,navigationFactory,$routeParams,UserRegistrationService){
        
        function init(){
            $scope.tabs=navigationFactory.getNavigationTabs();
            //console.log('tabs : '+JSON.stringify($scope.tabs));
            
            feedbackMe.setTabCss($scope.tabs,"login","home,register","logout,seek,userHome,provide");

            $scope.userLoggedIn = {};
            $scope.userLoggedIn.login = "Login";
            navigationFactory.setLoggedInUser($scope.userLoggedIn);
            //console.log('userLoggedIn : '+JSON.stringify($scope.userLoggedIn));
            
            //To show the activation success
            $scope.source = $routeParams.source;
            //console.log("Source is : "+$scope.source);
            if($scope.source === "activation"){
                $("#activationAlert").show();
            }else{
                $("#activationAlert").hide();
            }
            
            $scope.resetForm = {};
            $scope.resetEmail = $routeParams.resetEmail;
            $scope.resetActivationLink = $routeParams.activationLink;
            $scope.resetLinkReceived = false;
            if($scope.resetActivationLink){
                $scope.resetLinkReceived = true;
                $scope.resetForm.email = $scope.resetEmail;
                $scope.resetForm.activationKey = $scope.resetActivationLink;
                
                if($(".progress").length == 0){ //Hack to prevent duplicate progress bar for password strength
                $("#password").pwstrength({
                ui: {   
                        container: "#pwd-container",
                        showVerdictsInsideProgressBar: true,
                        showVerdicts:false,
                        viewports: {
                            progress: ".pwstrength_viewport_progress"
                            }
                        }
                    });
                }
                
                //Hide send link
                $("#sendLinkDiv").hide();
            }else{
                $("#resetDiv").hide();
            }
            
        }
        init();
      
        $scope.login = function(){
            if($scope.loginForm.$valid){
                
                UserRegistrationService.login($scope.userLoggedIn).success(function(data){
                    //console.log("Login successful : "+JSON.stringify(data));
                    var resString = JSON.stringify(data);
                    if(resString.indexOf("errors") > 0 ){                        
                        $scope.errorObject = showErrors(JSON.stringify(data));
                        $("#errorLoginAlert").show();
                        return;
                    }else{
                        var user = extractUser(data);
                        if(user){
                            $scope.userLoggedIn.userId = user.userId;
                            $scope.userLoggedIn.name = user.name;
                            $scope.userLoggedIn.isOrganization = user.isOrganization;
                            //console.log("Loggedin user is : "+$scope.userLoggedIn.name);
                            var isOrg = $scope.userLoggedIn.isOrganization;
                            //console.log("Is organization : "+isOrg);
                            navigationFactory.setLoggedInUser($scope.userLoggedIn);
                            $scope.tabs.userName = user.name;
                            feedbackMe.setTabCss($scope.tabs,"userHome","logout,seek","home,register,login");
                            if(!isOrg){
                                location.href = "#/accountSetup";
                            }else{
                                location.href = "#/userHome";
                            }                                
                        }else{
                            $scope.errorObject = "No User Found";
                            $("#errorLoginAlert").show();
                            return;
                        }                        
                    }                    
                    //return false;
                }).error(function(data){
                    //console.log("Registration failure"); 
                    $scope.errorObject = showErrors(JSON.stringify(data));
                    $("#errorLoginAlert").show();
                });
            }
        };
        
        $scope.sendPasswordLink = function(){
            if($scope.passwordForm.$valid){
                UserRegistrationService.sendPasswordLink($scope.resetForm.email).success(function(data){
                    //console.log("Login successful : "+JSON.stringify(data));
                    var resString = JSON.stringify(data);
                    if(resString.indexOf("errors") > 0 ){                        
                        $scope.errorObject = showErrors(JSON.stringify(data));
                        $("#errorResetAlert").show();
                        return;
                    }else{
                        $("#successResetAlert").show();
                    }                    
                    //return false;
                }).error(function(data){
                    //console.log("Registration failure"); 
                    $scope.errorObject = showErrors(JSON.stringify(data));
                    $("#errorResetAlert").show();
                });
            }
        };
        
        $scope.resetPassword = function(){
            if($scope.passwordForm.$valid){
                if(!($(".password-verdict").html() === 'Strong' || $(".password-verdict").html() === 'Very Strong')){
                    $("#password-strength-alert").show();
                    return;
                }else{
                    $("#password-strength-alert").hide();
                }
                if(!($("#password").val() === $("#retype-password").val())){
                    $("#passwordAlert").show();
                    return;
                }else{
                    $("#passwordAlert").hide();
                }
                
                UserRegistrationService.resetPassword($scope.resetForm).success(function(data){
                    //console.log("Login successful : "+JSON.stringify(data));
                    var resString = JSON.stringify(data);
                    if(resString.indexOf("errors") > 0 ){                        
                        $scope.errorObject = showErrors(JSON.stringify(data));
                        $("#errorResetAlert").show();
                        return;
                    }else{
                        $("#successResetCompleteAlert").show();
                    }                    
                    //return false;
                }).error(function(data){
                    //console.log("Registration failure"); 
                    $scope.errorObject = showErrors(JSON.stringify(data));
                    $("#errorResetAlert").show();
                });
            }
        }
        
        function showErrors(errorObject){
            var jsonObj = JSON.parse(errorObject);
            var errString = "";
            if(jsonObj){
                var errorArray = jsonObj.errors;
            //console.log("errrror array is : "+JSON.stringify(errorArray));
            
            for(var obj in errorArray){
                //console.log("errrror object is : "+JSON.stringify(obj));
                errString = errString+errorArray[obj]["errorDescription"]+" , ";
            }
            errString = errString.substr(0,errString.length-2);
            }
            return errString;
        }
        
        function extractUser(data){ 
            if(data){
                var dataArray = data.users;
                //console.log("data array is : "+JSON.stringify(dataArray));
                return dataArray[0];
            }
        }
    };
    
    LoginController.$inject = ['$scope','navigationFactory','$routeParams','UserRegistrationService'];    
   
    angular.module('feedbackApp').controller('LoginController',LoginController);
}());