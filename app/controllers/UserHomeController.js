(function(){

    var UserHomeController = function($scope,navigationFactory,$routeParams,FeedbackService){
        
        init();

        function init(){
             $scope.tabs=navigationFactory.getNavigationTabs();
            
             feedbackMe.setTabCss($scope.tabs,"userHome","logout,seek,provide","home,register,login");

             $scope.tabs.userName = '';
             $scope.userHomeForm = {};
             $scope.userHomeForm.showProductsTab = false;
            
             if(!navigationFactory.getLoggedInUser().userId){
                 location.href="#/login";
                 return;
             }else{
                 if(navigationFactory.getLoggedInUser().isOrganization === '1'){
                     $scope.userType = "4";
                     if(navigationFactory.getLoggedInUser().isProvider === true){
                        $scope.userHomeForm.showProductsTab = true;   
                     }                     
                 }else{
                     $scope.userType = "1";
                     $scope.userHomeForm.showProductsTab = false;
                 }
             }
             console.log("show products tab is "+$scope.userHomeForm.showProductsTab);
             $scope.userLoggedIn = navigationFactory.getLoggedInUser();
             $scope.tabs.userName = navigationFactory.getLoggedInUser().name;            
             //console.log('userLoggedIn : '+JSON.stringify($scope.userLoggedIn));
            
            fetchFeedbacks(navigationFactory.getLoggedInUser().userId,$scope.userType);
             if($scope.userHomeForm.showProductsTab === true){
                 fetchFeedbacks(navigationFactory.getLoggedInUser().userId,"2");
             }

             $('#feed-tabs a').click(function (e) {
                 e.preventDefault()
                 $(this).tab('show')
             });
             if($scope.userHomeForm.showProductsTab === false){
                 $('#products-tab').hide();
                 $('#products').hide();
             }
            //console.log("user home controller init");
        }
        
        
        
        function fetchFeedbacks(userId,targetTypeId){
            FeedbackService.fetchFeedbacks(userId,targetTypeId).success(function(data){
                var resString = JSON.stringify(data);
                if(resString.indexOf("errors") > 0 ){
                    $scope.errorObject = showErrors(JSON.stringify(data));
                        $("#errorProvideAlert").show();
                        return;  
                }else{
                    if(targetTypeId === "1" || targetTypeId === "4"){
                        $scope.userHomeForm.feedbacks = extractFeedbacks(data);    
                    }else{
                        $scope.userHomeForm.productFeedbacks = extractFeedbacks(data);
                    }                    
                    //$scope.userHomeForm.feedbackCategs = extractfeedbackCategs($scope.provideFeedbackForm.seekDetails.feedbackCategories);
                }                    
            }).error(function(data){
                $scope.errorObject = showErrors(JSON.stringify(data));
                $("#errorProvideAlert").show();
            });
        }
        
        function showErrors(errorObject){
            var jsonObj = JSON.parse(errorObject);
            var errString = "";
            if(jsonObj){
                var errorArray = jsonObj.errors;
            //console.log("errrror array is : "+JSON.stringify(errorArray));

            for(var obj in errorArray){
                //console.log("errrror object is : "+JSON.stringify(obj));
                errString = errString+errorArray[obj]["errorDescription"]+" , ";
            }
            errString = errString.substr(0,errString.length-2);
            }
            return errString;
        }

        function extractFeedbacks(data){ 
            if(data){
                var dataArray = data.feedbacks;
                //console.log("data array is : "+JSON.stringify(dataArray));
                return dataArray;
            }
        }
        
    };
    
    UserHomeController.$inject = ['$scope','navigationFactory','$routeParams','FeedbackService'];    
   
    angular.module('feedbackApp').controller('UserHomeController',UserHomeController);
}());