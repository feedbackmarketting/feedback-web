(function(){

    var IndexController = function($scope,navigationFactory){
        
        function init(){
            $scope.appName=navigationFactory.getAppName();
            $scope.tabs=navigationFactory.getNavigationTabs();
            //console.log('tabs : '+JSON.stringify($scope.tabs));
            feedbackMe.setTabCss($scope.tabs,"home","register,login","logout,seek,provide,userHome");  
            $scope.userLoggedIn = navigationFactory.getLoggedInUser();
           // console.log("user in home controller"+$scope.userLoggedIn);
        }        
        init();            
    };
    
    IndexController.$inject = ['$scope','navigationFactory'];    
   
    angular.module('feedbackApp').controller('IndexController',IndexController);
}());