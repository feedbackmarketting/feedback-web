(function(){
    var UserRegistrationController = function($scope,navigationFactory,UserRegistrationService){
        
        function init(){
            $scope.tabs=navigationFactory.getNavigationTabs();
            //console.log('tabs : '+JSON.stringify($scope.tabs));
            
            feedbackMe.setTabCss($scope.tabs,"register","home,login","seek,provide");

            $scope.user = {};
            $scope.user.register = "Register";
            if($(".progress").length == 0){ //Hack to prevent duplicate progress bar for password strength
                $("#password").pwstrength({
                ui: {   
                        container: "#pwd-container",
                        showVerdictsInsideProgressBar: true,
                        showVerdicts:false,
                        viewports: {
                            progress: ".pwstrength_viewport_progress"
                        }
                    }
                });
            }
            //console.log("register controller init");
        };
        init();
                
        $scope.register = function(){
            if($scope.userForm.$valid){
                
                if(!($(".password-verdict").html() === 'Strong' || $(".password-verdict").html() === 'Very Strong')){
                    $("#password-strength-alert").show();
                    return;
                }else{
                    $("#password-strength-alert").hide();
                }
                if(!($("#password").val() === $("#retype-password").val())){
                    $("#passwordAlert").show();
                    return;
                }else{
                    $("#passwordAlert").hide();
                }

                UserRegistrationService.register($scope.user).success(function(data){
                    //console.log("Registration successful : "+JSON.stringify(data));
                    var resString = JSON.stringify(data);
                    if(resString.indexOf("errors") > 0 ){                        
                        $scope.errorObject = showErrors(JSON.stringify(data));
                        $("#errorRegistrationAlert").show();
                        return;
                    }else{
                        location.href = "#/registerSuccess";
                    }                    
                    //return false;
                }).error(function(data){
                    //console.log("Registration failure"); 
                    $scope.errorObject = showErrors(JSON.stringify(data));
                    $("#errorRegistrationAlert").show();
                });
            }
        };
        
        function showErrors(errorObject){
            var jsonObj = JSON.parse(errorObject);
            var errString = "";
            if(jsonObj){
                var errorArray = jsonObj.errors;
            //console.log("errrror array is : "+JSON.stringify(errorArray));
            
            for(var obj in errorArray){
                //console.log("errrror object is : "+JSON.stringify(obj));
                errString = errString+errorArray[obj]["errorDescription"]+" , ";
            }
            errString = errString.substr(0,errString.length-2);
            }
            return errString;
        }
                
    };
    
    UserRegistrationController.$inject = ['$scope','navigationFactory','UserRegistrationService'];
       
    angular.module('feedbackApp').controller('UserRegistrationController',UserRegistrationController);
}());