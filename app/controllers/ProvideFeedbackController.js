    (function () {

        var ProvideFeedbackController = function ($scope, navigationFactory, $routeParams, ProductService,UserRegistrationService,FeedbackService) {

            function init() {
                $scope.tabs = navigationFactory.getNavigationTabs();
                
                feedbackMe.setTabCss($scope.tabs,"","home,register,login","logout");

                $scope.provider = $routeParams["provider"];
                $scope.seekId = $routeParams["seekId"];
                $scope.targetTypeId = $routeParams["targetTypeId"];
                $scope.provideFeedbackForm = {};
                $scope.provideFeedbackForm.seekDetails = {};
                $scope.provideFeedbackForm.feedbackCategs = [];
                $scope.provideFeedbackForm.overallFeedback = "";
                $scope.providerId = null;                

                $scope.action = "provideFeedback";
            };
            init();
            if(!navigationFactory.getLoggedInUser().userId){
                getProviderId();
            }
            fetchSeekDetails($scope.seekId,$scope.targetTypeId);

            $scope.provideFeedback = function(){
              if($scope.provideForm.$valid){
                  $("#errorProvideAlert").hide();
                  for(var i in $scope.provideFeedbackForm.feedbackCategs){
                      var cat = $scope.provideFeedbackForm.feedbackCategs[i];
                      if(cat.rating === 0){
                          $scope.errorObject = "The rating on each category is mandatory";
                          $("#errorProvideAlert").show();
                          return;
                      }
                  }

                  FeedbackService.provideFeedback($scope.providerId,$scope.provider,$scope.targetTypeId,$scope.provideFeedbackForm.seekDetails.productId,$scope.provideFeedbackForm.feedbackCategs,$scope.provideFeedbackForm.overallFeedback,$scope.seekId,$scope.provider,$scope.provideFeedbackForm.seekDetails.productName,$scope.provideFeedbackForm.seekDetails.seekerEmail).success(function(data){
                        var resString = JSON.stringify(data);
                        if(resString.indexOf("errors") > 0 ){
                            $scope.errorObject = showErrors(JSON.stringify(data));
                                $("#errorProvideAlert").show();
                                return;  
                        }else{     
                            location.href="#/provideFeedbackSuccess";
                            return;
                        }                    
                    }).error(function(data){
                        $scope.errorObject = showErrors(JSON.stringify(data));
                        $("#errorProvideAlert").show();
                    });
              }  
            };
            
            $scope.goHome = function(){
                if(!navigationFactory.getLoggedInUser().userId){
                    location.href="#/";
                }else{
                    location.href="#/userHome";
                }
            };

            function fetchSeekDetails(seekId,targetTypeId){
                FeedbackService.fetchSeekDetails(seekId,targetTypeId).success(function(data){
                    var resString = JSON.stringify(data);
                    if(resString.indexOf("errors") > 0 ){
                        $scope.errorObject = showErrors(JSON.stringify(data));
                            $("#errorProvideAlert").show();
                            return;  
                    }else{                            
                        $scope.provideFeedbackForm.seekDetails = extractSeekDetails(data);
                        $scope.provideFeedbackForm.feedbackCategs = extractfeedbackCategs($scope.provideFeedbackForm.seekDetails.feedbackCategories);
                    }                    
                }).error(function(data){
                    $scope.errorObject = showErrors(JSON.stringify(data));
                    $("#errorProvideAlert").show();
                });
            }

            function showErrors(errorObject){
                var jsonObj = JSON.parse(errorObject);
                var errString = "";
                if(jsonObj){
                    var errorArray = jsonObj.errors;
                //console.log("errrror array is : "+JSON.stringify(errorArray));

                for(var obj in errorArray){
                    //console.log("errrror object is : "+JSON.stringify(obj));
                    errString = errString+errorArray[obj]["errorDescription"]+" , ";
                }
                errString = errString.substr(0,errString.length-2);
                }
                return errString;
            }

            function extractSeekDetails(data){ 
                if(data){
                    var dataArray = data.seekDetails;
                    //console.log("data array is : "+JSON.stringify(dataArray));
                    return dataArray[0];
                }
            }

            function extractfeedbackCategs(data){ 
                if(data){
                    var dataArray = data.feedbackCategs;
                    for(var i in dataArray){
                        dataArray[i].rating=0;
                    }
                    //console.log("data array is : "+JSON.stringify(dataArray));
                    return dataArray;
                }
            }
            
            function getProviderId(){
                UserRegistrationService.searchUser($scope.provider).success(function(data){
                    var resString = JSON.stringify(data);
                    if(resString.indexOf("errors") > 0 ){
                        $scope.providerId = null;
                    }else{                  
                        var usr = extractUser(data);
                        if(usr){
                            $scope.providerId = usr.userId;    
                        }
                    }                    
                });
            }
            
            function extractUser(data){ 
                if(data){
                    var dataArray = data.users;
                    //console.log("data array is : "+JSON.stringify(dataArray));
                    return dataArray[0];
                }
            }
        };

        ProvideFeedbackController.$inject = ['$scope','navigationFactory','$routeParams','ProductService','UserRegistrationService','FeedbackService'];    

        angular.module('feedbackApp').controller('ProvideFeedbackController',ProvideFeedbackController);
    }());