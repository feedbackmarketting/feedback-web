    (function () {

        var ProvideVoluntaryFeedbackController = function ($scope, navigationFactory, $routeParams, ProductService,UserRegistrationService,FeedbackService) {

            var tagetTypeMapping = {"person":1,"product":2,"service":3,"organization":4};

            function init() {
                $scope.tabs = navigationFactory.getNavigationTabs();
                
                feedbackMe.setTabCss($scope.tabs,"provide","userHome,seek,logout","home,register,login");

                $scope.provideVolFeedbackForm = {};
                $scope.provideVolFeedbackForm.targetOptions=[{value:"organization",description:"Provide feedback on an oraganization"},
                                                            {value:"product",description:"Provide feedback on a product"},
                                                            {value:"service",description:"Provide feedback on a service"}];
                $scope.provideVolFeedbackForm.targetType="";
                $scope.provideVolFeedbackForm.organisations = fetchOrganisations("");
                $scope.provideVolFeedbackForm.products=[];               
                $scope.provideVolFeedbackForm.targetOrg="";
                $scope.provideVolFeedbackForm.targetProduct="";
                $scope.provideVolFeedbackForm.isProductOrService=false;
                $scope.provideVolFeedbackForm.isProduct=false;
                $scope.provideVolFeedbackForm.isService=false;
                $scope.provideVolFeedbackForm.feedbackCategs = [];
                $scope.provideVolFeedbackForm.overallFeedback = "";
                $scope.providerId = null;                

                $scope.action = "provideFeedback";
            };
            init();
            if(!navigationFactory.getLoggedInUser().userId){
                getProviderId();
            }else{
                $scope.providerId = navigationFactory.getLoggedInUser().userId;
            }

            $scope.selectTargetType = function(){
                if($scope.provideVolFeedbackForm.targetType === "product"){
                        $scope.provideVolFeedbackForm.isProductOrService=true;
                        $scope.provideVolFeedbackForm.isProduct=true;
                        $scope.fetchProducts();
                }else if($scope.provideVolFeedbackForm.targetType === "service"){
                    $scope.provideVolFeedbackForm.isProductOrService=true;
                    $scope.provideVolFeedbackForm.isService=true;
                    $scope.fetchProducts();
                }else{
                    $scope.provideVolFeedbackForm.isProductOrService=false;
                }
                getStdCategs($scope.provideVolFeedbackForm.targetType);
            }

            $scope.fetchProducts = function(){
                //console.log("The selected to organisation is "+$scope.provideVolFeedbackForm.targetOrg);
                var isService = $scope.provideVolFeedbackForm.targetType === 'service'?1:0;
                //console.log("is service local variable "+isService);
                $scope.provideVolFeedbackForm.targetProduct = "";
                fetchProducts($scope.provideVolFeedbackForm.targetOrg,isService);
            }

            $scope.fetchCategories = function(){
                getStdCategs($scope.provideVolFeedbackForm.targetType);
            }

            $scope.addCategory = function(){
                //console.log("add categ");
                var len = $scope.provideVolFeedbackForm.feedbackCategs.length; 
                $scope.provideVolFeedbackForm.feedbackCategs[len] = {"name":"","isNew":true};
                //console.log(JSON.stringify($scope.provideVolFeedbackForm.feedbackCategs));
            }

            $scope.removeCateg = function(catg){
                //console.log("remove categ");
                var indx = $scope.provideVolFeedbackForm.feedbackCategs.indexOf(catg);
                $scope.provideVolFeedbackForm.feedbackCategs.splice(indx,1);
            }

            $scope.provideFeedback = function(){
              if($scope.provideForm.$valid){
                  $("#errorProvideAlert").hide();
                  for(var i in $scope.provideVolFeedbackForm.feedbackCategs){
                      var cat = $scope.provideVolFeedbackForm.feedbackCategs[i];
                      if(cat.rating === 0){
                          $scope.errorObject = "The rating on each category is mandatory";
                          $("#errorProvideAlert").show();
                          return;
                      }
                  }
                  //console.log("target type" + $scope.provideVolFeedbackForm.targetType);
                  //console.log(tagetTypeMapping[$scope.provideVolFeedbackForm.targetType]);
                  var targetTypeId;
                  if($scope.provideVolFeedbackForm.targetType === "organization"){
                      targetTypeId = $scope.provideVolFeedbackForm.targetOrg;
                  }else{
                      targetTypeId = $scope.provideVolFeedbackForm.targetProduct;
                  }

                  FeedbackService.provideFeedback($scope.providerId,navigationFactory.getLoggedInUser().email,
                                    tagetTypeMapping[$scope.provideVolFeedbackForm.targetType],targetTypeId,
                                    $scope.provideVolFeedbackForm.feedbackCategs,
                                    $scope.provideVolFeedbackForm.overallFeedback).success(function(data){
                        var resString = JSON.stringify(data);
                        if(resString.indexOf("errors") > 0 ){
                            $scope.errorObject = showErrors(JSON.stringify(data));
                                $("#errorProvideAlert").show();
                                return;  
                        }else{     

                            location.href="#/provideFeedbackSuccess";
                            return;
                        }                    
                    }).error(function(data){
                        $scope.errorObject = showErrors(JSON.stringify(data));
                        $("#errorProvideAlert").show();
                    });
              }  
            };
            
            $scope.goHome = function(){
                if(!navigationFactory.getLoggedInUser().userId){
                    location.href="#/";
                }else{
                    location.href="#/userHome";
                }
            };

            function getStdCategs(target){
                ProductService.getStdCategs(target).success(function(data){
                    var resString = JSON.stringify(data);
                    if(resString.indexOf("errors") > 0 ){
                        if(resString.indexOf("NO_DATA_FOUND") > 0){
                            //$scope.seekFeedbackForm.products = [];
                        }else{
                            $scope.errorObject = showErrors(JSON.stringify(data));
                            $("#errorSeekAlert").show();
                            return;    
                        }
                    }else{                            
                        $scope.provideVolFeedbackForm.feedbackCategs = extractCategs(data);                                                      
                    }                    
                //return false;
                }).error(function(data){
                    //console.log("Registration failure"); 
                    $scope.errorObject = showErrors(JSON.stringify(data));
                    $("#errorSeekAlert").show();
                });
            }

            function extractCategs(data){ 
                if(data){
                    var dataArray = data.feedbackCategs;
                    //console.log("data array is : "+JSON.stringify(dataArray));
                    return dataArray;
                }
            }

                function fetchSeekDetails(seekId,targetTypeId){
                    FeedbackService.fetchSeekDetails(seekId,targetTypeId).success(function(data){
                        var resString = JSON.stringify(data);
                        if(resString.indexOf("errors") > 0 ){
                            $scope.errorObject = showErrors(JSON.stringify(data));
                                $("#errorProvideAlert").show();
                                return;  
                        }else{                            
                            $scope.provideVolFeedbackForm.seekDetails = extractSeekDetails(data);
                            $scope.provideVolFeedbackForm.feedbackCategs = extractfeedbackCategs($scope.provideVolFeedbackForm.seekDetails.feedbackCategories);
                        }                    
                    }).error(function(data){
                        $scope.errorObject = showErrors(JSON.stringify(data));
                        $("#errorProvideAlert").show();
                    });
                }

                function fetchOrganisations(name){
                    FeedbackService.searchOrganisation(name).success(function(data){
                        var resString = JSON.stringify(data);
                        if(resString.indexOf("errors") > 0 ){
                            $scope.errorObject = showErrors(JSON.stringify(data));
                                $("#errorProvideAlert").show();
                                return;  
                        }else{                            
                            $scope.provideVolFeedbackForm.organisations = extractUsers(data);
                        }                    
                    }).error(function(data){
                        $scope.errorObject = showErrors(JSON.stringify(data));
                        $("#errorProvideAlert").show();
                    });
                }

            function fetchProducts(userId,isService){
                ProductService.getUserProducts(userId,isService).success(function(data){
                var resString = JSON.stringify(data);
                if(resString.indexOf("errors") > 0 ){
                    if(resString.indexOf("NO_DATA_FOUND") > 0){
                        $scope.provideVolFeedbackForm.products = [];
                    }else{
                        $scope.errorObject = showErrors(JSON.stringify(data));
                        $("#errorConfigAlert").show();
                        return;    
                    }
                }else{                            
                    $scope.provideVolFeedbackForm.products = extractProducts(data);                                                      
                }                    
                //return false;
                }).error(function(data){
                    //console.log("Registration failure"); 
                    $scope.errorObject = showErrors(JSON.stringify(data));
                    $("#errorConfigAlert").show();
                });
            }

            function extractProducts(data){ 
                if(data){
                    var dataArray = data.products;
                    //console.log("data array is : "+JSON.stringify(dataArray));
                    return dataArray;
                }
            }            

            function showErrors(errorObject){
                var jsonObj = JSON.parse(errorObject);
                var errString = "";
                if(jsonObj){
                    var errorArray = jsonObj.errors;
                //console.log("errrror array is : "+JSON.stringify(errorArray));

                for(var obj in errorArray){
                    //console.log("errrror object is : "+JSON.stringify(obj));
                    errString = errString+errorArray[obj]["errorDescription"]+" , ";
                }
                errString = errString.substr(0,errString.length-2);
                }
                return errString;
            }

            function extractSeekDetails(data){ 
                if(data){
                    var dataArray = data.seekDetails;
                    //console.log("data array is : "+JSON.stringify(dataArray));
                    return dataArray[0];
                }
            }

            function extractfeedbackCategs(data){ 
                if(data){
                    var dataArray = data.feedbackCategs;
                    for(var i in dataArray){
                        dataArray[i].rating=0;
                    }
                    //console.log("data array is : "+JSON.stringify(dataArray));
                    return dataArray;
                }
            }
            
            function getProviderId(){
                UserRegistrationService.searchUser($scope.provider).success(function(data){
                    var resString = JSON.stringify(data);
                    if(resString.indexOf("errors") > 0 ){
                        $scope.providerId = null;
                    }else{                  
                        var usr = extractUser(data);
                        if(usr){
                            $scope.providerId = usr.userId;    
                        }
                    }                    
                });
            }
            
            function extractUser(data){ 
                if(data){
                    var dataArray = data.users;
                    //console.log("data array is : "+JSON.stringify(dataArray));
                    return dataArray[0];
                }
            }

            function extractUsers(data){ 
                if(data){
                    var dataArray = data.users;
                    //console.log("data array is : "+JSON.stringify(dataArray));
                    return dataArray;
                }
            }
        };

        ProvideVoluntaryFeedbackController.$inject = ['$scope','navigationFactory','$routeParams','ProductService','UserRegistrationService','FeedbackService'];    

        angular.module('feedbackApp').controller('ProvideVoluntaryFeedbackController',ProvideVoluntaryFeedbackController);
    }());