(function(){

    var HomeController = function($scope,navigationFactory){
        
        function init(){
            $scope.appName=navigationFactory.getAppName();
            $scope.tabs=navigationFactory.getNavigationTabs();
            //console.log('tabs : '+JSON.stringify($scope.tabs));
            //activeTabs = home
            //inActiveTabs = register,login,
            //hiddenTabs = logout,seek,userHome
            feedbackMe.setTabCss($scope.tabs,"home","register,login","logout,seek,userHome,provide");  
            $scope.userLoggedIn = navigationFactory.getLoggedInUser();
           //console.log("home controller init");
        }                
        init();            
    };
    
    HomeController.$inject = ['$scope','navigationFactory'];    
   
    angular.module('feedbackApp').controller('HomeController',HomeController);
}());