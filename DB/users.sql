-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 04, 2016 at 02:47 PM
-- Server version: 5.5.45-cll-lve
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `feedbackme`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `name` varchar(50) NOT NULL COMMENT 'Social name',
  `email` varchar(50) NOT NULL COMMENT 'Unique email id',
  `mobile` varchar(20) DEFAULT NULL COMMENT 'Unique mobile number',
  `password` varchar(100) NOT NULL COMMENT 'Encrypted password',
  `terms_agreed` tinyint(1) NOT NULL COMMENT 'Whether user agreed the terms and conditions',
  `created_date` datetime NOT NULL COMMENT 'Created date and time',
  `updated_date` timestamp NOT NULL DEFAULT NOW() COMMENT 'updated date and time',
  `activation_key` varchar(75) NOT NULL COMMENT 'Activation key used for user activation',
  `active` boolean not null DEFAULT false COMMENT 'Whether the user user is activated',
  `is_organization` tinyint(1) COMMENT 'Whether the user is an organization true/false',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `mobile` (`mobile`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COMMENT='Stores the details of the registered user' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `feedback_targets`
--

CREATE TABLE IF NOT EXISTS `feedback_targets` (
  `id` tinyint NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `name` varchar(30) NOT NULL COMMENT 'Target name',  
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)  
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COMMENT='Stores the targets on which the feedback is applied' AUTO_INCREMENT=1 ;

--
-- Default targets of the system
--

INSERT INTO `feedback_targets` (`name`) VALUES ('PERSON');
INSERT INTO `feedback_targets` (`name`) VALUES ('PRODUCT');
INSERT INTO `feedback_targets` (`name`) VALUES ('SERVICE');
INSERT INTO `feedback_targets` (`name`) VALUES ('ORGANIZATION');
-- --------------------------------------------------------

--
-- Table structure for table `feedback_categories`
--

CREATE TABLE IF NOT EXISTS `feedback_categories` (
  `id` int(20) NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `name` varchar(500) NOT NULL COMMENT 'Category name',
  `target` tinyint NOT NULL COMMENT 'Foreign key from feedback_targets table',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  FOREIGN KEY (`target`) REFERENCES feedback_targets (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COMMENT='Stores the specific categories on which the feedback is sought' AUTO_INCREMENT=1 ;

--
-- Default categories of the system
--
-- For a person
INSERT INTO `feedback_categories` (`name`,`target`) VALUES ('Attitude',1);
INSERT INTO `feedback_categories` (`name`,`target`) VALUES ('Soft skills',1);
INSERT INTO `feedback_categories` (`name`,`target`) VALUES ('Networking skills',1);
INSERT INTO `feedback_categories` (`name`,`target`) VALUES ('Business knowledge',1);
INSERT INTO `feedback_categories` (`name`,`target`) VALUES ('Technical knowledge',1);

-- For a product
INSERT INTO `feedback_categories` (`name`,`target`) VALUES ('Price',2);
INSERT INTO `feedback_categories` (`name`,`target`) VALUES ('Quality',2);
INSERT INTO `feedback_categories` (`name`,`target`) VALUES ('Warrenty',2);
INSERT INTO `feedback_categories` (`name`,`target`) VALUES ('Fit for purpose',2);
INSERT INTO `feedback_categories` (`name`,`target`) VALUES ('Durability',2);

-- For a service
INSERT INTO `feedback_categories` (`name`,`target`) VALUES ('Promptness service',3);
INSERT INTO `feedback_categories` (`name`,`target`) VALUES ('Quality of service',3);
INSERT INTO `feedback_categories` (`name`,`target`) VALUES ('Friendlyness of the service provider',3);
INSERT INTO `feedback_categories` (`name`,`target`) VALUES ('Cost of the service',3);
INSERT INTO `feedback_categories` (`name`,`target`) VALUES ('Completeness of the service',3);

-- For a organization
INSERT INTO `feedback_categories` (`name`,`target`) VALUES ('Culture of the organization',4);
INSERT INTO `feedback_categories` (`name`,`target`) VALUES ('Commitment of the organization',4);
INSERT INTO `feedback_categories` (`name`,`target`) VALUES ('Credibility of the organization',4);
INSERT INTO `feedback_categories` (`name`,`target`) VALUES ('Accredition of the organization',4);
INSERT INTO `feedback_categories` (`name`,`target`) VALUES ('Popularity of the organization',4);

--
-- Table structure for table `product_categories`
--

CREATE TABLE IF NOT EXISTS `product_categories` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `name` varchar(30) NOT NULL COMMENT 'Category name',  
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)  
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COMMENT='Stores the product categories' AUTO_INCREMENT=1 ;

--
-- Table structure for `products`
--
CREATE TABLE IF NOT EXISTS `products` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `name` varchar(100) NOT NULL COMMENT 'Product name',
  `is_service` boolean NOT NULL COMMENT 'Is this a service?',
  `specification` varchar(2000) COMMENT 'Specification of the product or service stored as a JSON string',
  `category` int COMMENT 'Product category',
  `created_date` datetime NOT NULL COMMENT 'Created date and time',
  `updated_date` timestamp NOT NULL DEFAULT NOW() COMMENT 'updated date and time',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  FOREIGN KEY (`category`) REFERENCES product_categories (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COMMENT='Stores the products' AUTO_INCREMENT=1 ;

--
-- Table structure for `user_products`
--
CREATE TABLE IF NOT EXISTS `user_products` (
  `user` bigint(20) NOT NULL COMMENT 'Foreign key from users table',
  `product` bigint NOT NULL COMMENT 'Foreign key from products table',
  `created_date` datetime NOT NULL COMMENT 'Created date and time',
  `updated_date` timestamp NOT NULL DEFAULT NOW() COMMENT 'updated date and time',  
  UNIQUE KEY `user_product_fb_categ` (`user`,`product`),
  FOREIGN KEY (`user`) REFERENCES users (`id`),
  FOREIGN KEY (`product`) REFERENCES products (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COMMENT='Stores the user product mapping.' AUTO_INCREMENT=1 ;

--
-- Table structure for `seek_feedback`
--
CREATE TABLE IF NOT EXISTS `seek_feedback` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `seeker` bigint(20) NOT NULL COMMENT 'Foreign key from users table',
  `target_type` tinyint NOT NULL COMMENT 'Foreign key from feedback_targets table',
  `target` bigint NOT NULL COMMENT 'Target subjects id based on the target type. If target type is person/organization then this will be user_id else if target type is product/service then this will be product_id',
  `created_date` datetime NOT NULL COMMENT 'Created date and time',
  `updated_date` timestamp NOT NULL DEFAULT NOW() COMMENT 'updated date and time',
  PRIMARY KEY (`id`),
  FOREIGN KEY (`seeker`) REFERENCES users (`id`),
  FOREIGN KEY (`target_type`) REFERENCES feedback_targets (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COMMENT='Stores the products' AUTO_INCREMENT=1 ;

--
-- Table structure for `seek_categories`
--
CREATE TABLE IF NOT EXISTS `seek_categories` (
  `seek_id` bigint NOT NULL COMMENT 'Foreign key from seek_feedback',
  `feedback_category` varchar(500) COMMENT 'Feedback category on which the feedback is sought',
  FOREIGN KEY (`seek_id`) REFERENCES seek_feedback (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COMMENT='Stores mapping between the seek and categories sought' AUTO_INCREMENT=1 ;

--
-- Table structure for `seek_audience`
--
CREATE TABLE IF NOT EXISTS `seek_audience` (
  `seek_id` bigint NOT NULL COMMENT 'Foreign key from seek_feedback',
  `email` varchar(50) COMMENT 'email address of the target audience',
  `is_complete` boolean COMMENT 'Has the target audience given the feedback?',
  FOREIGN KEY (`seek_id`) REFERENCES seek_feedback (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COMMENT='Stores the mapping between a seek and its audiences' AUTO_INCREMENT=1 ;

--
-- Table structure for `provide_feedback`
--
CREATE TABLE IF NOT EXISTS `provide_feedback` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `provider` bigint(20) COMMENT 'Foreign key from users table',
  `provider_email` varchar(50) NOT NULL COMMENT 'Email address of the provider. This is to capture the feedback from non-registered users',
  `target_type` tinyint NOT NULL COMMENT 'Foreign key from feedback_targets table',
  `target` bigint NOT NULL COMMENT 'Target subjects id based on the target type. If target type is person/organization then this will be user_id else if target type is product/service then this will be product_id',
  `comment`varchar(2000) COMMENT 'Optional comments about the target',
  `created_date` datetime NOT NULL COMMENT 'Created date and time',
  `updated_date` timestamp NOT NULL DEFAULT NOW() COMMENT 'updated date and time',
  PRIMARY KEY (`id`),
  FOREIGN KEY (`provider`) REFERENCES users (`id`),
  FOREIGN KEY (`target_type`) REFERENCES feedback_targets (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COMMENT='Stores the feedback provided by the users' AUTO_INCREMENT=1 ;

--
-- Table structure for `provide_categories`
--
CREATE TABLE IF NOT EXISTS `provide_categories` (
  `provide_id` bigint NOT NULL COMMENT 'Foreign key from provide_feedback',
  `feedback_category` varchar(500) NOT NULL COMMENT 'Feedback category on which the feedback is sought',
  `rating` decimal(2,1) NOT NULL COMMENT 'Rating on number of stars in 1 to 5 scale',
  `comment`varchar(2000) COMMENT 'Optional comments about the category',
  FOREIGN KEY (`provide_id`) REFERENCES provide_feedback (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COMMENT='Stores the mapping between feedback provided and categories' AUTO_INCREMENT=1 ;