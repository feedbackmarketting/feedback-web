//Register user and No-registered has given feedback about an organization
select feedback.id as feedback_id,provider.name as provider_name, feedback.provider_email,feedback.comment,feedbackCateg.feedback_category,feedbackCateg.rating,feedbackCateg.comment as reason
from users provider,provide_feedback feedback,provide_categories feedbackCateg
where provider.id=feedback.provider and feedback.id=feedbackCateg.provide_id and feedback.target_type=4 and feedback.target=3
union
select feedback.id as feedback_id,feedback.provider_email as provider_name, feedback.provider_email,feedback.comment,feedbackCateg.feedback_category,feedbackCateg.rating,feedbackCateg.comment as reason
from provide_feedback feedback,provide_categories feedbackCateg
where feedback.id=feedbackCateg.provide_id and feedback.target_type=4 and feedback.target=3 and feedback.provider is null;

select feedback.id as feedbackId,provider.name as providerName, feedback.provider_email as providerEmail,feedback.comment,feedbackCateg.feedback_category,feedbackCateg.rating,feedbackCateg.comment as reason from users provider,provide_feedback feedback,provide_categories feedbackCateg where provider.id=feedback.provider and feedback.id=feedbackCateg.provide_id and feedback.target_type=4 and feedback.target=3 union select feedback.id as feedback_id,feedback.provider_email as provider_name, feedback.provider_email,feedback.comment,feedbackCateg.feedback_category,feedbackCateg.rating,feedbackCateg.comment as reason from provide_feedback feedback,provide_categories feedbackCateg where feedback.id=feedbackCateg.provide_id and feedback.target_type=4 and feedback.target=3 and feedback.provider is null;

--For Users
select feedback.id as feedbackId,provider.name as providerName, feedback.provider_email as providerEmail,feedback.comment from users provider,provide_feedback feedback where provider.id=feedback.provider and feedback.target_type=$targetTypeId and feedback.target=$userId union select feedback.id as feedback_id,feedback.provider_email as provider_name, feedback.provider_email,feedback.comment from provide_feedback feedback where feedback.target_type=$targetTypeId and feedback.target=$userId and feedback.provider is null order by feedbackId desc;

select feedbackCateg.feedback_category,feedbackCateg.rating,feedbackCateg.comment as reason from provide_categories feedbackCateg where feedbackCateg.provide_id = 1;

--For Products
select feedback.id as feedbackId,provider.name as providerName, feedback.provider_email as providerEmail,feedback.comment,product.name from users provider,provide_feedback feedback,products product,user_products userProduct where provider.id=feedback.provider and feedback.target_type in (2,3) and feedback.target=product.id and product.id=userProduct.product and userProduct.user=1 union select feedback.id as feedback_id,feedback.provider_email as provider_name, feedback.provider_email,feedback.comment,product.name from provide_feedback feedback,products product,user_products userProduct where feedback.target_type in (2,3) and feedback.target=product.id and product.id=userProduct.product and userProduct.user=1 and feedback.provider is null order by feedbackId desc;;