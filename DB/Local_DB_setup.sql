Local Machine setup
===================
mysql -u root -p
pwd : <rootpassword>

create database feedbackme;
create user 'feedbackme_php'@'localhost' identified by '<database_password>';
grant all on feedbackme.* to 'feedback_me'@'localhost';
grant all privileges on feedbackme.* to 'feedbackme_php'@'localhost' with grant option;
flush privileges;

SHOW DATABASES;

use feedbackme;

SHOW tables;
