$(document).ready(function(){

	$("#registerButton").on("click", function(){
		var valid = true;		
		if($("#userName").val() && $("#password").val() && $("#retype-password").val()){
			var termsAgreed = $("#termsAgreed").is(":checked");
			if(!($("#email").val() || $("#mobileNo").val())){
				valid = false;
				$("#emailAlert").show();
			}else{
				$("#emailAlert").hide();
			}

			if(!($("#password").val() === $("#retype-password").val())){
				valid = false;
				$("#passwordAlert").show();
			}else{
				$("#passwordAlert").hide();
			}

			if(!termsAgreed){
				valid = false;
				$("#termsAlert").show();
			}else{
				$("#termsAlert").hide();
			}

			if(!valid){
				return false;
			}else{
				$.ajax({
					method: "POST",
					url: "http://localhost/FeedbackMe/php/users.php",
					data: {"register":"Register", "userName": $("#userName").val(), "email":$("#email").val(), "mobile":$("#mobileNo").val(), "password":$("#password").val(),"termsAgreed":termsAgreed}
				}).done(function(data){
					$("#errorRegistrationAlert").hide();
					console.log("Registration successful : "+data);
					location.href="#registrationSuccess";
					return false;
				}).fail(function(data){
					$("#errorRegistrationAlert").show();
					for(i in data){
						console.log(JSON.stringify(data));	
					}
					return false;					
				});
			}
		}
		return false;
	});
});