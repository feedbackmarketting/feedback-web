describe('login page test',function(){

	beforeEach(function(){
		browser.get('http://localhost/FeedbackMe/#/login');
	});

	var emailTxt = element(by.id('email'));
	var passwordTxt = element(by.id('password'));
	var loginBtn = element(by.id('login-button'));

	it('should login',function(){
		emailTxt.sendKeys('balaji.t321@gmail.com');
		passwordTxt.sendKeys('Ramajayam1$');
		loginBtn.click();
		expect(element(by.id('feed-tabs')).isPresent()).toBe(true);
	});
	
});