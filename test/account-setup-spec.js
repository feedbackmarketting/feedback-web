describe('account setup page test',function(){

	beforeEach(function(){
		browser.get('http://localhost/FeedbackMe/#/accountSetup/test');
	});

	var accountTypeDrpDn = element(by.id('account-type'));
	var saveBtn = element(by.id('save-setup'));

	it('should setup account for pesonal user',function(){
		// Option 1 : Personal account
		selectDropDown(accountTypeDrpDn,1);
		saveBtn.click();

		expect(element(by.id('products-tab')).getAttribute('style')).toEqual('display: none;');
		expect(element(by.id('person-tab')).isPresent()).toBe(true);
	});

	it('should setup account for organisational user with products and services',function(){
		// Option 1 : Personal account
		selectDropDown(accountTypeDrpDn,2);
		expect(element(by.id('is-provider')).isPresent()).toBe(true);
		var isProvider = element(by.id('is-provider'));
		//Option 1 : select 'Yes we have products and services'
		selectDropDown(isProvider,1);
		saveBtn.click();
		expect(element(by.id('product-config-hd')).isPresent()).toBe(true);
		expect(element(by.id('new-product-btn')).isPresent()).toBe(true);
		expect(element(by.id('cofig-prod-btn')).isPresent()).toBe(true);
		var configProdBtn = element(by.id('cofig-prod-btn'));

	});

	it('should setup account for organisational user without products and services',function(){
		// Option 1 : Personal account
		selectDropDown(accountTypeDrpDn,2);
		expect(element(by.id('is-provider')).isPresent()).toBe(true);
		var isProvider = element(by.id('is-provider'));
		//Option 1 : select 'No we dont have products and services'
		selectDropDown(isProvider,2);
		saveBtn.click();
		expect(element(by.id('products-tab')).getAttribute('style')).toEqual('display: none;');
		expect(element(by.id('person-tab')).isPresent()).toBe(true);


		//expect(element(by.id('products-tab')).isPresent()).toBe(true);
		//expect(element(by.id('person-tab')).isPresent()).toBe(true);
	});

	var selectDropDown = function(element,optionIndex){
		if(optionIndex){
			var options = element.all(by.tagName('option'));

			options.then(function(options){
				options[optionIndex].click();
			});
		}
	}
});