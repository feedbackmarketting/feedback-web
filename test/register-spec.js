describe('register page test',function(){
	
	beforeEach(function(){
		browser.get('http://localhost/FeedbackMe/#/register');
	});

	it('shoud show brand',function(){
		expect(element(by.binding('appName')).getText()).toEqual('ShowMyFeedback.com');
	});

	it('shoud show home,register and login tabs',function(){
		expect(element(by.id('home-tab')).getAttribute('class')).toEqual('');
		expect(element(by.id('register-tab')).getAttribute('class')).toEqual('active');
		expect(element(by.id('login-tab')).getAttribute('class')).toEqual('');
	});

	it('shoud hide user-home,seek-feedback,provide-feedback and logout tabs',function(){
		expect(element(by.id('user-home-tab')).getAttribute('class')).toEqual('hidden');
		expect(element(by.id('seek-feedback-tab')).getAttribute('class')).toEqual('hidden');
		expect(element(by.id('logout-tab')).getAttribute('class')).toEqual('hidden');
	});

	it('shoud show input fields',function(){
		expect(element(by.id('user-name')).getTagName()).toBe('input');
		expect(element(by.id('email')).getTagName()).toBe('input');
		expect(element(by.id('mobile')).getTagName()).toBe('input');
		expect(element(by.id('password')).getTagName()).toBe('input');
		expect(element(by.id('retype-password')).getTagName()).toBe('input');
		expect(element(by.id('terms-agreed')).getTagName()).toBe('input');
	});	

	var registerBtn = element(by.id('register-button'));
	var userNameTxt = element(by.id('user-name'));
	var emailTxt = element(by.id('email'));
	var passwordTxt = element(by.id('password'));
	var retypePwdTxt = element(by.id('retype-password'));
	var termsAgreedChk = element(by.id('terms-agreed'));

	it('should validate input',function(){		
		registerBtn.click();
		var unpro = userNameTxt.getAttribute('class');//Promis
		unpro.then(function(cls){
			expect(cls.indexOf('ng-invalid')).not.toBe(-1);
		});
		userNameTxt.sendKeys("test user name");
		registerBtn.click();
		emailTxt.getAttribute('class').then(function(cls){
			expect(cls.indexOf('ng-invalid')).not.toBe(-1);
		});
		emailTxt.sendKeys("invalid email");
		registerBtn.click();
		emailTxt.getAttribute('class').then(function(cls){
			expect(cls.indexOf('ng-invalid')).not.toBe(-1);
		});
		emailTxt.sendKeys("testvalidemail@domain.com");
		registerBtn.click();
		passwordTxt.getAttribute('class').then(function(cls){
			expect(cls.indexOf('ng-invalid')).not.toBe(-1);
		});
		passwordTxt.sendKeys("test password");
		registerBtn.click();
		retypePwdTxt.getAttribute('class').then(function(cls){
			//expect(cls.indexOf('ng-invalid')).not.toBe(-1); //Not working as not bound to ng model
		});
		retypePwdTxt.sendKeys("test password");
		registerBtn.click();
		termsAgreedChk.getAttribute('class').then(function(cls){
			expect(cls.indexOf('ng-invalid')).not.toBe(-1);
		});
		termsAgreedChk.click();
		registerBtn.click();
		element(by.id('password-strength-alert')).getAttribute('style').then(function(cls){
			expect(cls.indexOf('display: block')).toBe(-1);
		});

		passwordTxt.sendKeys("StrongPassword1$");
		retypePwdTxt.sendKeys("StrongPassword1$");
		registerBtn.click();
		element(by.id('password-strength-alert')).getAttribute('style').then(function(cls){
			//expect(cls.indexOf('display: block')).not.toBe(-1);
		});			
	});

	it('should register a user',function(){
		userNameTxt.sendKeys("test user name");
		emailTxt.sendKeys("testvalidemail@domain.com");
		passwordTxt.sendKeys("StrongPassword1$");
		retypePwdTxt.sendKeys("StrongPassword1$");
		termsAgreedChk.click();
		registerBtn.click();
		expect(element(by.id('register-success-title')).getText()).toEqual('Thanks for registering with us!');

		browser.ignoreSynchronization = true;
		browser.get('http://localhost/FeedbackMe/php/users.php?deleteUser=yes&email=testvalidemail@domain.com');//Delete the created user
		browser.ignoreSynchronization = false;
	});

});