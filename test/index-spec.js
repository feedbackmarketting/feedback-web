describe('index page test',function(){
	
	beforeEach(function(){
		browser.get('http://localhost/FeedbackMe');
	});

	it('shoud show brand',function(){
		expect(element(by.binding('appName')).getText()).toEqual('ShowMyFeedback.com');
	});

	it('shoud show home,register and login tabs',function(){
		expect(element(by.id('home-tab')).getAttribute('class')).toEqual('active');
		expect(element(by.id('register-tab')).getAttribute('class')).toEqual('');
		expect(element(by.id('login-tab')).getAttribute('class')).toEqual('');
	});

	it('shoud hide user-home,seek-feedback,provide-feedback and logout tabs',function(){
		expect(element(by.id('user-home-tab')).getAttribute('class')).toEqual('hidden');
		expect(element(by.id('seek-feedback-tab')).getAttribute('class')).toEqual('hidden');
		expect(element(by.id('logout-tab')).getAttribute('class')).toEqual('hidden');
	});

	it('shoud show learn more link',function(){
		var learnMoreLink = element(by.id('learn-more-link'));
		learnMoreLink.click();
		expect(element(by.id('what-is-title')).getText()).toEqual('What is ShowMyFeedback.com?');
	});	

});