exports.config = {
	seleniumAddress: 'http://localhost:4444/wd/hub',
	specs: [
	'index-spec.js',
	'register-spec.js',
	'login-spec.js',
	'account-setup-spec.js'
		],
	capabilities:{
		browserName:'firefox'
	}
	//multiCapabilities: [
		//{browserName:'firefox'},
	//	{browserName:'chrome'}
		//,{browserName:'ie'}
	//]
};